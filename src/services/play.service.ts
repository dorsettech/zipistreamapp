import { Injectable, Inject } from "@angular/core";
import { Router } from "@angular/router";
import { Platform } from "@ionic/angular";
import { Media, MediaObject } from "@ionic-native/media/ngx";
import { Constants } from "../models/contants.models";
import { Subject } from "rxjs";
import { ApiService } from "./api.service";
// import { MusicControls } from "@ionic-native/music-controls/ngx";

declare let cordova: any;
declare var window: any;
@Injectable({
  providedIn: "root",
})
export class PlayService {
  mediaTimer: any;
  memoMedia: MediaObject;
  message: any = "";
  i = 0;
  private changeSong = new Subject<any>();
  private durationChanged = new Subject<any>();
  private pausePlay = new Subject<any>();
  currentPlayingSong: any = "";
  isSpotifyUrl: boolean = false;
  isPlaying: boolean = false;
  constructor(
    // public media: Media,
    @Inject(Media) public media,
    public constant: Constants,
    public navCtrl: Router,
    public api: ApiService,
    // public musicControls: MusicControls,
    // @Inject(MusicControls) public musicControls,
    public platform: Platform // private musicControls: MusicControls
  ) {
    var that = this;

    // window.MusicControls.subscribe((action) => {
    //   console.log("Action:: ", action);
    //   const message = JSON.parse(action).message;
    //   switch (message) {
    //     case "music-controls-next":
    //       // Do something
    //       that.playNext();
    //       that.pausePlay.next(true);
    //       break;
    //     case "music-controls-previous":
    //       // Do something
    //       break;
    //     case "music-controls-pause":
    //       // Do something
    //       that.pauseSong();
    //       break;
    //     case "music-controls-play":
    //       // Do something
    //       that.isPlaying = true;
    //       that.playSong(that.constant.currentSong);
    //       break;
    //     case "music-controls-destroy":
    //       // Do something
    //       // navigator["app"].exitApp();
    //       break;

    //     // External controls (iOS only)
    //     case "music-controls-toggle-play-pause":
    //       // Do something
    //       break;
    //     case "music-controls-seek-to":
    //       const seekToInSeconds = JSON.parse(action).position;
    //       window.MusicControls.updateElapsed({
    //         elapsed: seekToInSeconds,
    //         isPlaying: true,
    //       });
    //       // Do something
    //       break;
    //     case "music-controls-skip-forward":
    //       // Do something
    //       break;
    //     case "music-controls-skip-backward":
    //       // Do something
    //       break;

    //     // Headset events (Android only)
    //     // All media button events are listed below
    //     case "music-controls-media-button":
    //       // Do something
    //       break;
    //     case "music-controls-headset-unplugged":
    //       // Do something
    //       that.pauseSong();
    //       break;
    //     case "music-controls-headset-plugged":
    //       // Do something
    //       break;
    //     default:
    //       break;
    //   }
    // })


    // window.MusicControls.subscribe().subscribe((action) => {
    //   console.log("Action:: ", action);
    //   const message = JSON.parse(action).message;
    //   switch (message) {
    //     case "music-controls-next":
    //       // Do something
    //       that.playNext();
    //       that.pausePlay.next(true);
    //       break;
    //     case "music-controls-previous":
    //       // Do something
    //       break;
    //     case "music-controls-pause":
    //       // Do something
    //       that.pauseSong();
    //       break;
    //     case "music-controls-play":
    //       // Do something
    //       that.isPlaying = true;
    //       that.playSong(that.constant.currentSong);
    //       break;
    //     case "music-controls-destroy":
    //       // Do something
    //       // navigator["app"].exitApp();
    //       break;

    //     // External controls (iOS only)
    //     case "music-controls-toggle-play-pause":
    //       // Do something
    //       break;
    //     case "music-controls-seek-to":
    //       const seekToInSeconds = JSON.parse(action).position;
    //       window.MusicControls.updateElapsed({
    //         elapsed: seekToInSeconds,
    //         isPlaying: true,
    //       });
    //       // Do something
    //       break;
    //     case "music-controls-skip-forward":
    //       // Do something
    //       break;
    //     case "music-controls-skip-backward":
    //       // Do something
    //       break;

    //     // Headset events (Android only)
    //     // All media button events are listed below
    //     case "music-controls-media-button":
    //       // Do something
    //       break;
    //     case "music-controls-headset-unplugged":
    //       // Do something
    //       that.pauseSong();
    //       break;
    //     case "music-controls-headset-plugged":
    //       // Do something
    //       break;
    //     default:
    //       break;
    //   }
    // });

  }

  /**
   * Function is not used anywhere
   * To get current action of music controls
   * @param action 
   */
  musicevents(action) {
    var that = this;
    console.log("Action:: ", action, that);
    const message = JSON.parse(action).message;
    switch (message) {
      case "music-controls-next":
        // Do something
        that.playNext();
        that.pausePlay.next(true);
        break;
      case "music-controls-previous":
        // Do something
        break;
      case "music-controls-pause":
        // Do something
        that.pauseSong();
        break;
      case "music-controls-play":
        // Do something
        that.isPlaying = true;
        console.log('Current song from music control:: ', that.constant.currentSong);
        that.playSong(that.constant.currentSong);
        break;
      case "music-controls-destroy":
        // Do something
        // navigator["app"].exitApp();
        break;

      // External controls (iOS only)
      case "music-controls-toggle-play-pause":
        // Do something
        break;
      case "music-controls-seek-to":
        // const seekToInSeconds = JSON.parse(action).position;
        // window.MusicControls.updateElapsed({
        //   elapsed: seekToInSeconds,
        //   isPlaying: true,
        // });
        // Do something
        break;
      case "music-controls-skip-forward":
        // Do something
        break;
      case "music-controls-skip-backward":
        // Do something
        break;

      // Headset events (Android only)
      // All media button events are listed below
      case "music-controls-media-button":
        // Do something
        break;
      case "music-controls-headset-unplugged":
        // Do something
        that.pauseSong();
        break;
      case "music-controls-headset-plugged":
        // Do something
        break;
      default:
        break;
    }
  }

  /**
   * To capture song change events
   */
  songChange(): Subject<any> {
    return this.changeSong;
  }

  /**
   * To capture duration change events
   */
  durationChange(): Subject<any> {
    return this.durationChanged;
  }

  /**
   * To capture pause play events
   */
  pauseAndPlay(): Subject<any> {
    return this.pausePlay;
  }

  /**
   * Play the song
   * @param file
   */
  playSong(file) {
    console.log("Play song:: ", file);
    if (this.mediaTimer != null) {
      clearInterval(this.mediaTimer);
      this.mediaTimer = null;
    }
    // this.constant.currentsongDuration = 0.00;
    // this.constant.songDuration = 0.00;
    if (!this.memoMedia) {
      this.isPlaying = false;
      this.pausePlay.next(false);
      this.constant.showBuffer = true;
      // this.constant.currentsongDuration = 0.00;
      // this.constant.songDuration = 0.00;
      this.i = 0;
      if (window.MusicControls) {
        // window.MusicControls.destroy();
      }
      this.playMusicControls(file);

      this.memoMedia = this.media.create(file.songUrl);
      console.log("created media :: ", this.memoMedia);
      // this.pausePlay.next(true);
    }

    this.memoMedia.onSuccess.subscribe((success) => {
      console.log("Media success :: ", success + "\n");
    });

    this.memoMedia.onError.subscribe((error) => {
      console.log("Media error :: ", JSON.stringify(error) + "\n");
      clearInterval(this.mediaTimer);
      if (error["code"] !== 1) {
        this.constant.play = false;
        this.isPlaying = true;
        this.playSong(this.constant.currentSong);
      }
    });

    this.memoMedia.onStatusUpdate.subscribe((status) => {
      console.log("status:: ", status);
      this.constant.status = status.toString();

      if (status.toString() == "1") {
        this.constant.isPlaying = false;
        this.constant.play = false;
      }

      if (status.toString() == "2") {
        setTimeout(() => {
          var that = this;
          this.currentPlayingSong = file;
          this.constant.isPlaying = true;
          let duration2 = this.memoMedia.getDuration();
          console.log("duration 2:: ", duration2);
          this.constant.dur = duration2;
          this.constant.songDuration = (duration2 / 60).toFixed(2);
          window.MusicControls.updateIsPlaying(true);

          this.isPlaying = true;
          if (this.i == 0) {
            console.log("pauseplay event hit:: ", this.i);
            this.pausePlay.next(true);
          }

          if (this.i > 0) {
            // setTimeout(() => {
            this.constant.play = true;
            // }, 200);
          }
          this.mediaTimer = setInterval(function () {
            if (that.memoMedia) {
              if (status.toString() == "2") {
                that.memoMedia
                  .getCurrentPosition()
                  .then((val: any) => {
                    if (val > -1 && duration2 > -1) {
                      // console.log("Val:: ", val, " - ", duration2);
                      if (val >= duration2) {
                        that.autoplayNextSong();
                        return false;
                      }
                      // that.constant.currentsongDuration = 0.0;
                      that.constant.currentsongDuration = (val / 60).toFixed(2);
                      that.constant.curDur = val;
                      that.durationChanged.next("");
                    }
                  })
                  .catch((err: any) => {
                    console.log("Current err:: ", err);
                  });
              }
            }
          }, 500);
          this.i = this.i + 1;
        }, 200);
      }

      if (status.toString() == "3") {
        setTimeout(() => {
          this.constant.play = false;
        }, 100);
      }

      if (status.toString() == "4") {
        console.log("Current duration:: ", this.constant.currentsongDuration);
        console.log("duration:: ", this.constant.songDuration);
        console.log("Song:: ", this.memoMedia);
        this.constant.play = false;
        this.constant.isPlaying = false;
        clearInterval(this.mediaTimer);
        this.autoplayNextSong();
        // if (this.constant.songDuration !== 0) {
        //   console.log("::1::");
        //   if (
        //     this.constant.currentsongDuration == this.constant.songDuration ||
        //     this.constant.currentsongDuration ==
        //       (this.constant.songDuration - 0.01).toFixed(2) ||
        //     this.constant.currentsongDuration >= this.constant.songDuration
        //   ) {
        //     console.log("::2::");
        //     clearInterval(this.mediaTimer);
        //     this.constant.play = false;
        //     this.stopSong();
        //     this.constant.currentsongDuration = 0.0;
        //     this.constant.songDuration = 0.0;
        //     setTimeout(() => {
        //       console.log("::3::");
        //       this.constant.isPlaying = false;
        //       if (
        //         this.constant.currentSongIndex <=
        //         this.constant.songList.length - 1
        //       ) {
        //         console.log("::4::", this.constant.isPlayerVisible);
        //         if (this.constant.isPlayerVisible) {
        //           this.changeSong.next("");
        //         } else {
        //           this.nextSong();
        //         }
        //       }
        //     }, 500);
        //   }
        // } else if (this.constant.currentsongDuration !== 0) {
        //   clearInterval(this.mediaTimer);
        //   this.constant.play = false;
        //   this.stopSong();
        //   this.constant.currentsongDuration = 0.0;
        //   this.constant.songDuration = 0.0;
        //   setTimeout(() => {
        //     this.constant.isPlaying = false;
        //     if (
        //       this.constant.currentSongIndex <=
        //       this.constant.songList.length - 1
        //     ) {
        //       this.constant.play = false;
        //       this.changeSong.next("");
        //     }
        //   }, 500);
        // }
      }
    });

    this.memoMedia.play();
    setTimeout(() => {
      this.constant.play = true;
    }, 1000);

    console.log('MusicControls:: ', window.MusicControls)
    if(window.MusicControls && window.MusicControls.hasOwnProperty('subscribe')) {
      console.log('::subscribe::');
      var that = this;
      window.MusicControls.subscribe((action) => {
        console.log('Subscribe action:: ', action);
        const message = JSON.parse(action).message;
        switch (message) {
          case "music-controls-next":
            // Do something
            that.playNext();
            that.pausePlay.next(true);
            break;
          case "music-controls-previous":
            // Do something
            break;
          case "music-controls-pause":
            // Do something
            that.pauseSong();
            break;
          case "music-controls-play":
            // Do something
            that.isPlaying = true;
            console.log('Current song from music control:: ', that.constant.currentSong);
            that.playSong(that.constant.currentSong);
            break;
          case "music-controls-destroy":
            // Do something
            // navigator["app"].exitApp();
            break;
    
          // External controls (iOS only)
          case "music-controls-toggle-play-pause":
            // Do something
            break;
          case "music-controls-seek-to":
            // const seekToInSeconds = JSON.parse(action).position;
            // window.MusicControls.updateElapsed({
            //   elapsed: seekToInSeconds,
            //   isPlaying: true,
            // });
            // Do something
            break;
          case "music-controls-skip-forward":
            // Do something
            break;
          case "music-controls-skip-backward":
            // Do something
            break;
    
          // Headset events (Android only)
          // All media button events are listed below
          case "music-controls-media-button":
            // Do something
            break;
          case "music-controls-headset-unplugged":
            // Do something
            that.pauseSong();
            break;
          case "music-controls-headset-plugged":
            // Do something
            break;
          default:
            break;
        }
      });
    }

    if(window.MusicControls && window.MusicControls.hasOwnProperty('listen')) {
      console.log('::listen::');
      window.MusicControls.listen(); // activates the observable above
    }
    
  }

  /**
   * Autoplay next song when song is ended.
   */
  autoplayNextSong() {
    if (this.constant.songDuration !== 0) {
      console.log("::1::");
      if (
        this.constant.currentsongDuration == this.constant.songDuration ||
        this.constant.currentsongDuration ==
          (this.constant.songDuration - 0.01).toFixed(2) ||
        this.constant.currentsongDuration >= this.constant.songDuration
      ) {
        console.log("::2::");
        clearInterval(this.mediaTimer);
        this.constant.play = false;
        this.stopSong();
        this.constant.currentsongDuration = 0.0;
        this.constant.songDuration = 0.0;
        setTimeout(() => {
          console.log("::3::");
          this.constant.isPlaying = false;
          if (
            this.constant.currentSongIndex <=
            this.constant.songList.length - 1
          ) {
            console.log("::4::", this.constant.isPlayerVisible);
            if (this.constant.isPlayerVisible) {
              this.changeSong.next("");
            } else {
              this.nextSong();
            }
          }
        }, 500);
      }
    } else if (this.constant.currentsongDuration !== 0) {
      clearInterval(this.mediaTimer);
      this.constant.play = false;
      this.stopSong();
      this.constant.currentsongDuration = 0.0;
      this.constant.songDuration = 0.0;
      setTimeout(() => {
        this.constant.isPlaying = false;
        if (
          this.constant.currentSongIndex <=
          this.constant.songList.length - 1
        ) {
          this.constant.play = false;
          this.changeSong.next("");
        }
      }, 500);
    }
  }

  /**
   * Music controls on notification area
   * @param file
   */
  playMusicControls(file) {
    try {
      setTimeout(() => {
        window.MusicControls.create({
          track: file.post_title, // optional, default : ''
          artist: file.authorName ? file.authorName : "", // optional, default : ''
          cover: file.image ? file.image : "assets/images/r1.png", // optional, default : nothing
          // cover can be a local path (use fullpath 'file:///storage/emulated/...', or only 'my_image.jpg' if my_image.jpg is in the www folder of your app)
          //           or a remote url ('http://...', 'https://...', 'ftp://...')
          isPlaying: true, // optional, default : true
          dismissable: false, // optional, default : false

          // hide previous/next/close buttons:
          hasPrev: true, // show previous button, optional, default: true
          hasNext: true, // show next button, optional, default: true
          hasClose: true, // show close button, optional, default: false

          // iOS only, optional
          // album: "Absolution", // optional, default: ''
          // duration: this.constant.songDuration, // optional, default: 0
          // elapsed: this.constant.currentsongDuration, // optional, default: 0
          hasSkipForward: false, // show skip forward button, optional, default: false
          hasSkipBackward: false, // show skip backward button, optional, default: false
          skipForwardInterval: 15, // display number for skip forward, optional, default: 0
          skipBackwardInterval: 15, // display number for skip backward, optional, default: 0
          hasScrubbing: false, // enable scrubbing from control center and lockscreen progress bar, optional

          // Android only, optional
          // text displayed in the status bar when the notification (and the ticker) are updated, optional
          ticker: 'Now playing "' + file.post_title + '"',
          // All icons default to their built-in android equivalents
          playIcon: "media_play",
          pauseIcon: "media_pause",
          prevIcon: "media_prev",
          nextIcon: "media_next",
          closeIcon: "media_close",
          notificationIcon: "notification",
        });
      }, 200);
    } catch {
      console.log("Error in init music controls");
    }
  }

  /**
   * Seek song
   * @param seekto
   */
  seekSong(seekto: any) {
    // console.log("seekTo:: ", seekto, "-", seekto * 60, "-", seekto * 60 * 1000);
    if (this.constant.status == "2" || this.constant.status == "3") {
      this.memoMedia.seekTo(seekto * 60 * 1000);
    }
  }

  /**
   * Pause the song
   */
  pauseSong() {
    console.log(" ::Pause song:: ");
    if (this.memoMedia) {
      this.constant.play = false;
      window.MusicControls.updateIsPlaying(false);
      this.memoMedia.pause();
    }
  }

  /**
   * Stop the music
   */
  stopSong() {
    if (this.memoMedia) {
      this.constant.play = false;
      clearInterval(this.mediaTimer);
      var a = this.memoMedia.stop();
      console.log(" ::stop song:: ", this.memoMedia);
      console.log(" ::stop song timer:: ", this.mediaTimer);
      this.memoMedia.release();
      this.memoMedia = null;
      console.log(" ::stop song timer 1:: ", this.mediaTimer);
      console.log(" ::stoptoped:: ", this.memoMedia);
      this.mediaTimer = null;
    }
  }

  /**
   * Play next song
   */
  playNext() {
    if (this.constant.isPlayerVisible) {
      this.changeSong.next("");
    } else {
      this.nextSong();
    }
  }

  /**
   * Play next song automatically when player screen is minimized
   */
  nextSong() {
    this.stopSong();
    console.log("currentSongIndex:: ", this.constant.currentSongIndex);
    if (this.constant.songList.length > 1) {
      if (this.constant.currentSongIndex == this.constant.songList.length - 1) {
        this.constant.currentSongIndex = -1;
      }
      this.constant.currentSongIndex = this.constant.currentSongIndex + 1;
      console.log("currentSongIndex + 1 :: ", this.constant.currentSongIndex);
      this.constant.currentSong = this.constant.songList[
        this.constant.currentSongIndex
      ];
      localStorage.setItem(
        "currentSong",
        JSON.stringify(this.constant.currentSong)
      );
      localStorage.setItem(
        "currentSongIndex",
        JSON.stringify(this.constant.currentSongIndex)
      );
      this.constant.songDuration = 0.0;
      this.constant.currentsongDuration = 0.0;
      this.constant.play = true;
      this.playSong(this.constant.currentSong);
    }
  }
}
