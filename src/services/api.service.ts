import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpClientModule,
  HttpHandler,
} from "@angular/common/http";
import { Http, HttpModule, RequestOptions, Headers } from "@angular/http";
import {
  AlertController,
  LoadingController,
  ToastController,
} from "@ionic/angular";
import "rxjs/add/operator/map";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  url = "https://zipi.honesttech.co.uk/";
  public loading: any = null;
  constructor(
    public http: Http,
    public HttpC: HttpClient,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public toastController: ToastController
  ) {}

  /**
   * Function to show the loader
   */
  async presentLoading() {
    if (this.loading) {
      this.hideLoading();
    }
    this.loading = await this.loadingController.create({
      spinner: "crescent",
      message: "Loading...",
      translucent: true,
      cssClass: "custom-class custom-loading",
      backdropDismiss: false,
    });
    await this.loading.present();
  }

  /**
   * Function to hide the loader
   */
  async hideLoading() {
    this.loadingController.getTop().then((loader) => {
      if (loader) {
        loader.dismiss();
      }
    });

    setTimeout(() => {
      this.loading
      .dismiss()
      .then(() => {
        // console.log("Loading dismissed!");
      })
      .catch((err) => {
        console.log("Loading error", err);
        setTimeout(() => {
          this.hideLoading();
        }, 2000);
      });
    }, 1000);

    setTimeout(() => {
      if(this.loading) {
        this.hideLoading();
      }
    }, 15000);

  }

  /**
   * Function to show the loader
   * @param status 
   * @param message 
   */
  async presentToast(status: string, message: string) {
    const toast = await this.toastController.create({
      message: message,
      color: status,
      duration: 3000,
      position: "bottom",
      mode: "md",
    });
    toast.present();
  }

  /**
   * Function to get nonce
   * @param url
   * @param params
   */
  public getNonce(url: any, params: any) {
    const headers = new Headers();
    const options = new RequestOptions({
      headers: headers,
      params,
    });
    return this.http.get(this.url + url, options).map((res) => res.json());
  }

  /**
   * POST method api
   * @param url
   * @param params
   */
  public post(url, params: any) {
    // user_id: JSON.parse(localStorage.getItem("user")).user.id,
    if(localStorage.getItem("user") !== null) {
      params.user_id = JSON.parse(localStorage.getItem("user")).user.id;
    }
    const headers = new Headers();
    // headers.append('enctype', 'multipart/form-data; boundary=WebAppBoundary');
    // headers.append('Content-Type', 'multipart/form-data');
    // headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(this.url + url, JSON.stringify(params), options)
      .map((res) => res.json());
  }

  /**
   * Get Method api
   * @param url
   * @param params
   */
  public get(url, params: any) {
    if(localStorage.getItem("user") !== null) {
      params.user_id = JSON.parse(localStorage.getItem("user")).user.id;
    }
    const headers = new Headers();
    const options = new RequestOptions({
      headers: headers,
      params: params,
    });
    return this.http.get(this.url + url, options).map((res) => res.json());
  }

  /**
   * Get spotify data
   * @param url
   * @param params
   */
  public getSpotifyData(url, params) {
    const headers = new Headers();
    const options = new RequestOptions({
      headers: headers,
      params: params,
    });
    return this.http.get(url, options).map((res) => res.json());
  }
}
