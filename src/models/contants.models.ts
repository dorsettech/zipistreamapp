export class Constants {
    static KEY_DEFAULT_LANGUAGE: string = 'mv_dl';
    currentSong: any = {};
    currentSongIndex: any;
    songList: any = [];
    nextSong: any = '';
    prevSong: any = '';
    songDuration: any = 0.00;
    currentsongDuration: any = 0.00;
    play: boolean = false;
    dur: any = 0.00;
    curDur: any = 0.00;
    status: any = '';
    isPlaying: any = false;
    isPlayerVisible: boolean = false;
    showBuffer: boolean = false;
    // pause: boolean = true;

    resetVariables() {
        this.currentSong = {};
        this.currentSongIndex = 0;
        this.songList = [];
        this.nextSong = '';
        this.prevSong = '';
        this.songDuration = 0.00;
        this.currentsongDuration = 0.00;
        this.play = false;
        this.dur = 0.00;
        this.curDur = 0.00;
        this.status = '';
        this.isPlaying = false;
        this.isPlayerVisible = false;
        this.showBuffer = false;
    }
}