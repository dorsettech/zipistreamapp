import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PlaysongPage } from "../playsong/playsong.page";
import { ApiService } from "../../services/api.service";
import { Constants } from "../../models/contants.models";
import { Events } from "../../services/events";
import { NavigationExtras } from "@angular/router";
import { ModalController, NavController } from "@ionic/angular";

@Component({
  selector: "app-search",
  templateUrl: "./search.page.html",
  styleUrls: ["./search.page.scss"],
})
export class SearchPage implements OnInit {
  searchVal: string = "";
  searchRes: any = [];
  showRes: boolean = false;
  categories: any = [];
  showLoader: boolean = false;
  constructor(
    private modalController: ModalController,
    private route: Router,
    public apiService: ApiService,
    public constants: Constants,
    public events: Events,
    public navCtrl: NavController
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    console.log("SeacrhPage ionViewWillEnter");
    this.searchVal = '';
    this.searchRes = [];
    this.categories = [];
    this.getPlylistChannel(true);
    this.getCategories();
  }

  gotoSetting() {
    this.route.navigate(["./setting"]);
  }

  playsong() {
    this.modalController
      .create({ component: PlaysongPage })
      .then((modalElement) => {
        modalElement.present();
      });
  }

  // searchInput() {
  //   console.log("Search Val:: ", this.searchVal);
  //   if (this.searchVal.length) {
  //     // const url = 'api/get_search_results/';
  //     const url = "wp-json/wp/v2/get/search";
  //     // const params = {search: this.searchVal};
  //     const params = { query: this.searchVal };
  //     this.apiService.get(url, params).subscribe(
  //       (val: any) => {
  //         console.log("val::: ", val);
  //         if (val.status == "success") {
  //           this.searchRes = val.data && val.data.length ? val.data : [];
  //           if (this.searchRes.length) {
  //             this.showRes = true;
  //           } else {
  //             this.showRes = false;
  //           }
  //         } else {
  //           this.searchRes = [];
  //         }
  //         // this.searchRes = (val.posts && val.posts.length) ? val.posts: [];
  //         // if(this.searchRes.length) {
  //         //   this.showRes = true;
  //         // } else {
  //         //   this.showRes = false;
  //         // }
  //       },
  //       (err: any) => {
  //         console.log("search Err:: ", err);
  //         this.showRes = false;
  //       }
  //     );
  //   } else {
  //     this.searchRes = [];
  //     this.showRes = false;
  //     this.recentSearched();
  //   }
  // }

  searchInput() {
    // this.searchVal = '';
    console.log("Search Val:: ", this.searchVal);
    if (this.searchVal.length) {
      // const url = "wp-json/wp/v2/get/search";
      this.searchRes = [];
      this.showLoader = true;
      const url = 'wp-json/wp/v2/podcast/search';
      const params = { search: this.searchVal };
      this.apiService.get(url, params).subscribe(
        (val: any) => {
          console.log("val::: ", val);
          if (val.status == "success") {
            this.showLoader = false;
            this.searchRes = val.data && val.data.length ? val.data : [];
            if (this.searchRes.length) {
              this.showRes = true;
            } else {
              this.showRes = false;
            }
          } else {
            this.showLoader = false;
            this.searchRes = [];
          }
        },
        (err: any) => {
          this.showLoader = false;
          console.log("search Err:: ", err);
          this.showRes = false;
        }
      );
    } else {
      this.searchRes = [];
      this.showRes = false;
      this.getPlylistChannel(true);
    }
  }

  // recentSearched(load) {
  //   this.searchVal = '';
  //   if(load) {
  //     this.apiService.presentLoading();
  //   }
  //   // const url = "wp-json/wp/v2/recent/searched";
  //   const url = "wp-json/wp/v2/recent/searched/podcasts"
  //   const params = {
  //     user_id: JSON.parse(localStorage.getItem("user")).user.id,
  //   };
  //   this.apiService.get(url, params).subscribe(
  //     (val: any) => {
  //       console.log("val::: ", val);
  //       if(load) {
  //         this.apiService.hideLoading();
  //       }
  //       if (val.status == "success") {
  //         if (val.data.length) {
  //           this.searchRes = val.data;
  //         } else {
  //           this.searchRes = [];
  //         }
  //       } else {
  //         this.searchRes = [];
  //       }
  //     },
  //     (err: any) => {
  //       if(load) {
  //         this.apiService.hideLoading();
  //       }
  //       console.log("search Err:: ", err);
  //       this.searchRes = [];
  //     }
  //   );
  // }

  getPlylistChannel(load) {
    if (load) {
      this.apiService.presentLoading();
    }
    let url = "wp-json/wp/v2/channel/in/playlist";
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
    };
    console.log("PARAM:: ", param);
    this.apiService.get(url, param).subscribe(
      (data: any) => {
        if (load) {
          setTimeout(() => {
            this.apiService.hideLoading();
          }, 2000);
        }
        console.log("Result::", data);
        if (data.status == "error") {
          this.searchRes = [];
          this.apiService.presentToast("danger", data.message);
          return false;
        } else {
          var that = this;
          if (data.data && data.data.length) {
            this.searchRes = data.data;
          } else {
            this.searchRes = [];
          }
        }
      },
      (error: any) => {
        this.searchRes = [];
        if (load) {
          this.apiService.hideLoading();
        }
        console.log("Playlist Error:: ", error);
        this.apiService.presentToast("danger", error);
      },
      () => {
        console.log("Finally");
      }
    );
  }

  chooseSearch(res) {
    console.log(res);
    this.showRes = false;
    this.searchVal = res.post_title;
  }

  blurSearch() {
    // setTimeout(() => {
    //   this.showRes = false;
    // },100);
  }

  // getCategories() {
  //   let url = "wp-json/wp/v2/categories/";
  //   let params = {};
  //   this.apiService.get(url, params).subscribe(
  //     (data) => {
  //       console.log("categories data:: ", data);
  //       if (data.length) {
  //         this.categories = data;
  //       }
  //     },
  //     (err) => {
  //       console.log("categories error:: ", err);
  //     }
  //   );
  // }

  getCategories() {
    let url = "wp-json/wp/v2/podcast/categories/";
    let params = {};
    this.apiService.get(url, params).subscribe(
      (data) => {
        console.log(
          "categories data:: ",
          data,
          "\n Val:: ",
          Object.values(data.data)
        );
        if (data.data) {
          if (Object.keys(data.data).length) {
            this.categories = Object.values(data.data);
          }
          console.log("Data:: ", this.categories);
        }
      },
      (err) => {
        console.log("categories error:: ", err);
      }
    );
  }

  openCategory(cat) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        parentId: cat.parent_id,
        from: "category",
      },
    };
    this.navCtrl.navigateForward(["tabs/categories-songs"], navigationExtras);
  }

  // recentSearched() {
  //   this.searchRes = [];
  //   this.apiService.presentLoading();
  //   const url = "wp-json/wp/v2/recent/searched";
  //   const params = {
  //     user_id: JSON.parse(localStorage.getItem("user")).user.id,
  //   };
  //   this.apiService.get(url, params).subscribe(
  //     (val: any) => {
  //       console.log("val::: ", val);
  //       this.apiService.hideLoading();
  //       if (val.status == "success") {
  //         if (val.data.length) {
  //           this.searchRes = val.data;
  //         } else {
  //           this.searchRes = [];
  //         }
  //       } else {
  //         this.searchRes = [];
  //       }
  //     },
  //     (err: any) => {
  //       this.apiService.hideLoading();
  //       console.log("search Err:: ", err);
  //       this.searchRes = [];
  //     }
  //   );
  // }

  playSong(song, i) {
    console.log("Song:: ", song);
    this.constants.currentSong = song;
    this.constants.currentSongIndex = i;
    this.constants.songList = this.searchRes;
    setTimeout(() => {
      this.events.publish("songPlayed", song);
    }, 100);
    let url = "wp-json/wp/v2/search/clicked";
    let param: any = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: song.ID,
    };
    this.apiService.post(url, param).subscribe((data) => {
      // console.log(data);
      if (data.status == "success") {
        console.log("Song added to recent search");
      }
    });
    let url1 = "wp-json/wp/v2/song/played";
    let param1: any = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: song.ID,
    };
    this.apiService.get(url1, param1).subscribe((data) => {
      // console.log(data);
      if (data.status == "success") {
        console.log("Song added to recent played");
      }
    });
  }

  openPodcast(pod, i) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        parentId: pod.parent,
        name: pod.name,
        description: pod.description,
        taxonomy: pod.taxonomy,
        term_id: pod.term_id,
        from: "podcast",
        image: pod.image
      },
    };
    this.navCtrl.navigateForward(["tabs/podcast-songs"], navigationExtras);
  }

  addPodcastToPlaylist(song) {
    // this.searchVal = '';
    var res = false;
    setTimeout(() => {
      if(!res) {
        this.apiService.presentLoading();
      }
    }, 2000);
    console.log("searchVal:: ", this.searchVal);
    let url = "";
    if (song.isAddedToPlaylist == "yes") {
      url = "wp-json/wp/v2/remove/channel/from/playlist";
    } else {
      url = "wp-json/wp/v2/add/channel/to/playlist";
    }
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      series_id: song.term_id,
    };
    console.log("PARAM:: ", param);
    this.apiService.get(url, param).subscribe(
      (data: any) => {
        this.apiService.hideLoading();
        res = true;
        if (song.isAddedToPlaylist == "yes") {
          song.isAddedToPlaylist = "no";
        } else {
          song.isAddedToPlaylist = "yes";
        }
        console.log("Result::", data);
        if (data.status == "error") {
          this.apiService.presentToast("danger", data.message);
          return false;
        } else {
          // this.getPlylistChannel(true);
        }
      },
      (error: any) => {
        res = true;
        this.apiService.hideLoading();
        console.log("Playlist Error:: ", error);
        this.apiService.presentToast("danger", error);
      },
      () => {
        console.log("Finally");
      }
    );
  }
}
