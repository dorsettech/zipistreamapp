import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { StoriesPage } from "../stories/stories.page";
import { PlaysongPage } from "../playsong/playsong.page";
import { ApiService } from "src/services/api.service";
import { Constants } from "../../models/contants.models";
import { Events } from "../../services/events";
import { NavigationExtras } from "@angular/router";
import { ModalController, NavController } from "@ionic/angular";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit {
  loaderToShow: any;
  categories: any = [];
  featured: any = [];
  featuredPod: any = [];
  constructor(
    private route: Router,
    private modalController: ModalController,
    public api: ApiService,
    public constants: Constants,
    public events: Events,
    public navCtrl: NavController
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.categories = [];
    this.featured = [];
    this.featuredPod = [];
    this.getFeaturedEpisodes();
    // this.getFeaturedPodcasts();
    // this.getCategories();
  }

  getFeaturedEpisodes() {
    this.api.presentLoading();
    let url = "wp-json/wp/v2/featured/posts/";
    let params = {};
    this.api.get(url, params).subscribe(
      (data) => {
        this.getFeaturedPodcasts();
        // this.api.hideLoading();
        console.log("featured data:: ", data);
        if (data.data.length) {
          this.featured = data.data;
        }
      },
      (err) => {
        this.getFeaturedPodcasts();
        // this.api.hideLoading();
        console.log("featured error:: ", err);
      }
    );
  }

  getFeaturedPodcasts() {
    let url = "wp-json/wp/v2/featured/podcasts/";
    let params = {};
    this.api.get(url, params).subscribe(
      (data) => {
        this.getCategories();
        console.log("Podcasts data:: ", data);
        if (data.data) {
          if (Object.keys(data.data).length) {
            var that = this;
            Object.values(data.data).forEach((val) => {
              console.log("--: ", val);
              if (val != false) {
                that.featuredPod.push(val);
              }
            });
          }
          setTimeout(() => {
            console.log("Podcasts:::: ", this.featuredPod);
          }, 500);
        } else {
          this.featuredPod = [];
        }
      },
      (err) => {
        this.getCategories();
        // this.api.hideLoading();
        console.log("featured error:: ", err);
        this.featuredPod = [];
      }
    );
  }

  getCategories() {
    let url = "wp-json/wp/v2/podcast/categories/";
    let params = {};
    this.api.get(url, params).subscribe(
      (data) => {
        this.api.hideLoading();
        console.log(
          "categories data:: ",
          data,
          "\n Val:: ",
          Object.values(data.data)
        );
        if (data.data) {
          if (Object.keys(data.data).length) {
            this.categories = Object.values(data.data);
          }
          console.log("Data:: ", this.categories);
        }
      },
      (err) => {
        this.api.hideLoading();
        console.log("categories error:: ", err);
      }
    );
  }

  gotoSetting() {
    this.route.navigate(["./setting"]);
  }

  subscribe() {
    this.route.navigate(["./subscribe"]);
  }

  playsong(song, i) {
    console.log("Song:: ", song);
    this.constants.currentSong = song;
    this.constants.currentSongIndex = i;
    this.constants.songList = this.featured;
    this.events.publish("songPlayed", song);

    let url = "wp-json/wp/v2/song/played";
    let param: any = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: song.ID,
    };
    this.api.get(url, param).subscribe((data) => {
      // console.log(data);
      if (data.status == "success") {
        console.log("Song added to recent played");
      }
    });

    // this.modalController
    //   .create({ component: PlaysongPage })
    //   .then((modalElement) => {
    //     modalElement.present();
    //   });
  }

  openCategory(cat) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        parentId: cat.parent_id,
        from: "category",
      },
    };
    this.navCtrl.navigateForward(["tabs/categories-songs"], navigationExtras);
  }

  openPodcast(feature) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        parentId: feature.parent,
        name: feature.name,
        description: feature.description,
        taxonomy: feature.taxonomy,
        term_id: feature.term_id,
        from: "podcast",
        image: feature.image
      },
    };
    this.navCtrl.navigateForward(["tabs/podcast-songs"], navigationExtras);
  }

  stories() {
    this.modalController
      .create({ component: StoriesPage })
      .then((modalElement) => {
        modalElement.present();
      });
  }
}
