import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PodcastSongsPage } from './podcast-songs.page';

describe('PodcastSongsPage', () => {
  let component: PodcastSongsPage;
  let fixture: ComponentFixture<PodcastSongsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PodcastSongsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PodcastSongsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
