import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { PlaysongPage } from "../playsong/playsong.page";
import { ApiService } from "src/services/api.service";
import { Constants } from "../../models/contants.models";
import { NavController } from '@ionic/angular';
import { Events } from "../../services/events";

@Component({
  selector: 'app-podcast-songs',
  templateUrl: './podcast-songs.page.html',
  styleUrls: ['./podcast-songs.page.scss'],
})
export class PodcastSongsPage implements OnInit {
  term_id: any;
  podcast: any = {};
  songs: any = [];
  noRecord: boolean = true;
  constructor(
    private route: ActivatedRoute,
    public api: ApiService,
    public constants: Constants,
    public navCtrl: NavController,
    public events: Events
  ) {
    this.route.queryParams.subscribe(params => {
      console.log('params:: ', params);
        this.podcast.description = params["description"];
        this.podcast.parentId = params["parentId"];
        this.podcast.taxonomy = params["taxonomy"];
        this.podcast.name = params["name"];
        this.podcast.image = params["image"];
        this.podcast.term_id = parseInt(params["term_id"]);
        this.api.presentLoading();
        this.getSongs();
    });
    this.events.subscribe('podcast-songs-back', (data) => {
      // this.goBack();
      if(document.getElementById('backButton')) {
        document.getElementById('backButton').click();
      }
    });
  }

  ngOnInit() {
  }

  getSongs() {
    let url = "wp-json/wp/v2/podcast/songs/";
    let params = {term_id: this.podcast.term_id};
    this.api.get(url, params).subscribe(
      (data) => {
        this.api.hideLoading();
        console.log("Podcasts data:: ", data);
        if (data.data) {
          if(data.data.length) {
            this.noRecord = false;
            this.songs = data.data;
          } else {
            this.noRecord = true;
          }
        } else {
          this.songs = [];
          this.noRecord = true;
        }
      },
      (err) => {
        this.api.hideLoading();
        this.songs = [];
        console.log("featured error:: ", err);
      });
  }

  goBack() {
    this.navCtrl.back();
  }

  playSong(song, i) {
    console.log("Song:: ", song);
    this.constants.currentSong = song;
    this.constants.currentSongIndex = i;
    this.constants.songList = this.songs;
    this.events.publish("songPlayed", song);

    let url = "wp-json/wp/v2/song/played";
    let param: any = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: song.ID,
    };
    this.api.get(url, param).subscribe((data) => {
      if (data.status == "success") {
        console.log("Song added to recent played");
      }
    });
  }

  addToPlaylist(song) {
    var res = false;
    setTimeout(() => {
      if(!res) {
        this.api.presentLoading();
      }
    }, 2000);
    let url = "";
    if (song.isAddedToPlaylist == 'yes') {
      url = "wp-json/wp/v2/remove/from/playlist";
    } else {
      url = "wp-json/wp/v2/add/to/playlist";
    }
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: song.ID,
    };
    console.log("PARAM:: ", param);
    this.api.get(url, param).subscribe(
      (data: any) => {
        res = true;
        this.api.hideLoading();
        if (song.isAddedToPlaylist == 'yes') {
          song.isAddedToPlaylist = 'no';
        } else {
          song.isAddedToPlaylist = 'yes';
        }
        console.log("Result::", data);
        if (data.status == "error") {
          this.api.presentToast("danger", data.message);
          return false;
        } else {
         
        }
      },
      (error: any) => {
        res = true;
        this.api.hideLoading();
        console.log("Playlist Error:: ", error);
        this.api.presentToast("danger", error);
      },
      () => {
        console.log("Finally");
      }
    );
  }

}
