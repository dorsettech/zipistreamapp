import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PodcastSongsPage } from './podcast-songs.page';

const routes: Routes = [
  {
    path: '',
    component: PodcastSongsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PodcastSongsPageRoutingModule {}
