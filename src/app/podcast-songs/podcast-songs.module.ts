import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PodcastSongsPageRoutingModule } from './podcast-songs-routing.module';

import { PodcastSongsPage } from './podcast-songs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PodcastSongsPageRoutingModule
  ],
  declarations: [PodcastSongsPage]
})
export class PodcastSongsPageModule {}
