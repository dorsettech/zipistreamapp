import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { IonicModule } from "@ionic/angular";

import { SignInPageRoutingModule } from "./sign-in-routing.module";

import { SignInPage } from "./sign-in.page";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    ReactiveFormsModule,
    SignInPageRoutingModule,
  ],
  declarations: [SignInPage],
})
export class SignInPageModule {}
