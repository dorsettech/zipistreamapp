import { Component, OnInit, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import { NavController } from "@ionic/angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ApiService } from "src/services/api.service";
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook/ngx";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";

@Component({
  selector: "app-sign-in",
  templateUrl: "./sign-in.page.html",
  styleUrls: ["./sign-in.page.scss"],
})
export class SignInPage implements OnInit, AfterViewInit {
  loginForm: FormGroup;
  submitAttempt: boolean = false;

  constructor(
    private route: Router,
    private navCtrl: NavController,
    public formBuilder: FormBuilder,
    public api: ApiService,
    private googlePlus: GooglePlus,
    private fb: Facebook,
    private splashScreen: SplashScreen
  ) {
    this.loginForm = this.formBuilder.group({
      // username: ["", Validators.compose([Validators.required])],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      password: ["", Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    // this.splashScreen.hide();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.splashScreen.hide();
    }, 2000);
  }

  goTosignup() {
    // this.route.navigate(["./sign-up"]);
    this.navCtrl.navigateForward("/sign-up");
  }

  choose_languages() {
    this.navCtrl.navigateRoot(["/choose-languages"]);
  }

  forgot_password() {
    this.route.navigate(["/forgot-password"]);
  }

  skip() {
    this.navCtrl.navigateRoot("/tabs/podcast-home");
  }

  signin() {
    if (this.loginForm.valid) {
      this.api.presentLoading();

      const nonceUrl = "api/get_nonce/";
      var nonceParam: any = {
        controller: "user",
        method: "generate_auth_cookie",
        // callback: "angular.callbacks._1",
      };
      this.api.getNonce(nonceUrl, nonceParam).subscribe(
        (val: any) => {
          console.log("nonce val:: ", val);
          if (val.status == "ok") {
            let param: any = this.loginForm.value;
            param.nonce = val.nonce;
            // param.notify = 'no';
            param.insecure = "cool";
            let url = "api/user/generate_auth_cookie/";
            console.log("PARAM:: ", param);
            this.api.get(url, param).subscribe(
              (data: any) => {
                console.log("Result::", data);
                if (data.status == "error") {
                  this.api.presentToast("danger", data.error);
                  return false;
                }
                this.api.presentToast("success", "SignIn successfully.");
                this.api.hideLoading();
                localStorage.setItem("user", JSON.stringify(data));
                setTimeout(() => {
                  this.navCtrl.navigateRoot("/tabs/podcast-home");
                }, 800);
              },
              (error: any) => {
                console.log("SignUp Error:: ", error);
                this.api.hideLoading();
                this.api.presentToast("danger", error);
              },
              () => {
                this.api.hideLoading();
                console.log("Finally");
              }
            );
          } else {
            this.api.hideLoading();
            this.api.presentToast("danger", val.error);
          }
        },
        (err: any) => {
          this.api.hideLoading();
          this.api.presentToast("danger", err);
          console.log("nonce err:: ", err);
        }
      );
    }
    // // this.navCtrl.navigateRoot('/tabs/podcast-home');
  }

  async googleLogin() {
    this.api.presentLoading();
    this.googlePlus
      .logout()
      .then((val) => {
        this.gLogin();
      })
      .catch((err) => {
        this.gLogin();
      });
  }

  async facebookLogin() {
    this.api.presentLoading();
    this.fb
      .getLoginStatus()
      .then((val) => {
        if (val.status == "connected") {
          this.fb
            .logout()
            .then(() => {
              this.fbLogin();
            })
            .catch(() => {
              this.fbLogin();
            });
        } else {
          this.fbLogin();
        }
      })
      .catch(() => {
        this.fbLogin();
      });
  }

  async fbLogin() {
    this.fb
      .login(["public_profile", "email"])
      .then((res: FacebookLoginResponse) => {
        console.log("Logged into Facebook!", res);
        this.api.hideLoading();
        if (res.status == "connected") {
          this.fb
            .api("/me?fields=name,email", ["public_profile", "email"])
            .then((data) => {
              console.log("Facebook login res:: ", data);
              var param = {
                name: data.name,
                email: data.email,
                fbid: data.id,
                provider: "facebook",
              };
              this.socialLogin(param);
            })
            .catch((err) => {
              console.log("Facebook login err:: ", err);
            });
        }
      })
      .catch((e) => {
        this.api.hideLoading();
        this.api.presentToast("danger", JSON.stringify(e));
        console.log("Error logging into Facebook", e);
      });
  }

  async gLogin() {
    this.googlePlus
      .login({
        webClientId:
          "371093403246-7ogjq0qvlrftr59qpgirfpsbvdm5md4t.apps.googleusercontent.com",
        offline: true,
      })
      .then((user: any) => {
        this.api.hideLoading();
        console.log("Google login res:: ", user);
        var param = {
          name: user.displayName,
          email: user.email,
          googleid: user.userId,
          provider: "google",
        };
        this.socialLogin(param);
      })
      .catch((err) => {
        this.api.hideLoading();
        this.api.presentToast("danger", JSON.stringify(err));
        console.log("Google login err:: ", err);
      });
  }

  socialLogin(param: any) {
    this.api.presentLoading();
    let url = "wp-json/wp/v2/social/login";
    this.api.get(url, param).subscribe(
      (data: any) => {
        console.log("Result::", data);
        if (data.status == "error") {
          this.api.presentToast("danger", data.error);
          return false;
        }
        this.api.presentToast("success", "SignIn successfully.");
        this.api.hideLoading();
        localStorage.setItem("user", JSON.stringify(data));
        setTimeout(() => {
          this.navCtrl.navigateRoot("/tabs/podcast-home");
        }, 800);
      },
      (error: any) => {
        console.log("SignUp Error:: ", error);
        this.api.hideLoading();
        this.api.presentToast("danger", error);
      }
    );
  }
}
