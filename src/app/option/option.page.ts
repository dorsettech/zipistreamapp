import { Component, OnInit } from "@angular/core";
import { ModalController, NavParams } from "@ionic/angular";
import { PlayService } from "src/services/play.service";
import { Constants } from "src/models/contants.models";
import { ApiService } from "src/services/api.service";
import { Events } from "../../services/events";
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: "app-option",
  templateUrl: "./option.page.html",
  styleUrls: ["./option.page.scss"],
})
export class OptionPage implements OnInit {
  liked: boolean = false;
  isAddedToPlaylist: boolean = false;
  currentSong: any = {};
  currentSongIndex: any = 0;
  constructor(
    private modalController: ModalController,
    public playService: PlayService,
    public constants: Constants,
    public api: ApiService,
    public events: Events,
    private socialSharing: SocialSharing,
    public navParams: NavParams
  ) {
    this.currentSong = navParams.get('song');
    this.currentSongIndex = navParams.get('index');
    if (this.currentSong.isLiked == "yes") {
      this.liked = true;
    } else {
      this.liked = false;
    }

    if (this.currentSong.isAddedToPlaylist == "yes") {
      this.isAddedToPlaylist = true;
    } else {
      this.isAddedToPlaylist = false;
    }
  }

  ngOnInit() {}

  dismiss() {
    this.modalController.dismiss();
  }

  likeSong() {
    var res = false;
    setTimeout(() => {
      if(!res) {
        this.api.presentLoading();
      }
    }, 2000);
    let url = "";
    if (this.liked) {
      url = "wp-json/wp/v2/unlike/song";
      this.currentSong.isLiked = 'no';
      this.constants.songList[this.currentSongIndex].isLiked = "no";
      if(this.currentSong.ID == this.constants.currentSong.ID) {
        this.constants.currentSong.isLiked = 'no';
        this.constants.songList[this.currentSongIndex].isLiked = "no";
      }
    } else {
      url = "wp-json/wp/v2/like/song/";
      this.currentSong.isLiked = 'yes';
      this.constants.songList[this.currentSongIndex].isLiked = "yes";
      if(this.currentSong.ID == this.constants.currentSong.ID) {
        this.constants.currentSong.isLiked = "yes";
        this.constants.songList[this.currentSongIndex].isLiked = "yes";
      }
    }
    localStorage.setItem(
      "currentSong",
      JSON.stringify(this.constants.currentSong)
    );
    localStorage.setItem(
      "currentSongList",
      JSON.stringify(this.constants.songList)
    );
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: this.currentSong.ID,
    };
    console.log("PARAM:: ", param);
    this.api.get(url, param).subscribe(
      (data: any) => {
        res = true;
        this.events.publish('likeUnlike', {});
        console.log("Result::", data);
        if (data.status == "error") {
          this.api.presentToast("danger", data.message);
          return false;
        } else {
          if (this.liked) {
            this.liked = false;
          } else {
            this.liked = true;
          }
        }
        this.api.hideLoading();
      },
      (error: any) => {
        res = true;
        this.api.hideLoading();
        console.log("Like unlike Error:: ", error);
        this.api.presentToast("danger", error);
      },
      () => {
        res = true;
        this.api.hideLoading();
        console.log("Finally");
      }
    );
  }

  playlist() {
    var res = false;
    setTimeout(() => {
      if(!res) {
        this.api.presentLoading();
      }
    }, 2000);
    let url = "";
    if (this.isAddedToPlaylist) {
      url = "wp-json/wp/v2/remove/from/playlist";
      this.currentSong.isAddedToPlaylist = "no";
      this.constants.songList[this.currentSongIndex].isAddedToPlaylist = "no";
    } else {
      url = "wp-json/wp/v2/add/to/playlist";
      this.currentSong.isAddedToPlaylist = "yes";
      this.constants.songList[this.currentSongIndex].isAddedToPlaylist = "yes";
    }
    localStorage.setItem(
      "currentSong",
      JSON.stringify(this.currentSong)
    );
    localStorage.setItem(
      "currentSongList",
      JSON.stringify(this.constants.songList)
    );
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: this.currentSong.ID,
    };
    console.log("PARAM:: ", param);
    this.api.get(url, param).subscribe(
      (data: any) => {
        res = true;
        console.log("Result::", data);
        this.events.publish('playlistUpdate', {});
        if (data.status == "error") {
          this.api.presentToast("danger", data.message);
          return false;
        } else {
          if (this.isAddedToPlaylist) {
            this.isAddedToPlaylist = false;
          } else {
            this.isAddedToPlaylist = true;
          }
        }
        this.api.hideLoading();
      },
      (error: any) => {
        res = true;
        this.api.hideLoading();
        console.log("Playlist Error:: ", error);
        this.api.presentToast("danger", error);
      },
      () => {
        console.log("Finally");
      }
    );
  }

  playNext() {
    // this.playService.stopSong();
    // setTimeout(() => {
      this.playService.playNext();
    // }, 500);
  }

  share() {
    var options = {
      message: 'Listen to the song: '+this.currentSong.post_title+' at '+this.currentSong.guid + ' on ZpiStream', // not supported on some apps (Facebook, Instagram)
      // subject: 'the subject', // fi. for email
      // files: ['', ''], // an array of filenames either locally or remotely
      // url: this.currentSong.guid,
      chooserTitle: 'Pick an app', // Android only, you can override the default share sheet title
    };
    
    this.socialSharing.shareWithOptions(options).then((data) => {
      console.log('Data shared:: ', data);
    }).catch((err) => {
      console.log('Error in data sharing:: ', err);
    });
  }
}
