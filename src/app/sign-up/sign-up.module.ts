import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { IonicModule } from "@ionic/angular";
import { SignUpPageRoutingModule } from "./sign-up-routing.module";
import { SignUpPage } from "./sign-up.page";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    ReactiveFormsModule,
    SignUpPageRoutingModule,
  ],
  declarations: [SignUpPage],
})
export class SignUpPageModule {}
