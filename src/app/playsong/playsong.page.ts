import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  NgZone,
} from "@angular/core";
import {
  ModalController,
  AlertController,
  IonInfiniteScroll,
} from "@ionic/angular";
import { OptionPage } from "../option/option.page";
import { PlayService } from "src/services/play.service";
import { Constants } from "src/models/contants.models";
import { ApiService } from "src/services/api.service";
import { Events } from "../../services/events";
import { DomSanitizer } from "@angular/platform-browser";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";

declare var window: any;
@Component({
  selector: "app-playsong",
  templateUrl: "./playsong.page.html",
  styleUrls: ["./playsong.page.scss"],
})
export class PlaysongPage implements OnInit {
  @ViewChild("nextBtn", { static: false }) nextBtn: ElementRef<HTMLElement>;
  @ViewChild("seekrange", { static: false }) seekrange: ElementRef<HTMLElement>;
  // @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;
  fabAction = false;
  playsong: boolean = false;
  currentSongIndex = 0;
  liked: boolean = false;
  // songs: any = [];
  currentSong: any = {};
  isSpotifyUrl: boolean = false;
  nextEvent: boolean = false;
  songs: any = [
    "https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_700KB.mp3",
    "https://file-examples.com/wp-content/uploads/2017/11/file_example_MP3_5MG.mp3",
  ];
  comingUpnext: any = [];
  songLength: any = 0.0;
  currentDuration: any = 0.0;
  durationHtml: any = "";
  displayDur: any = 0.0;
  displayCurrentDur: any = 0.0;
  present: boolean = false;
  isPlaying: boolean = false;
  isPlayingFirst: boolean = false;
  page: any = 1;
  total: any = 0;
  constructor(
    private modalController: ModalController,
    public playService: PlayService,
    public constants: Constants,
    public api: ApiService,
    public events: Events,
    public sanitizer: DomSanitizer,
    private socialSharing: SocialSharing,
    public alert: AlertController,
    public zone: NgZone
  ) {
    var that = this;
    this.playService.songChange().subscribe((val: any) => {
      console.log("-:songChange:-");
      that.isPlaying = false;
      that.isPlayingFirst = false;
      that.present = false;
      that.triggerNextClick();
    });
  }

  ngOnInit() {
    this.getCurrentSong();

    this.playService.durationChange().subscribe((val: any) => {
      setTimeout(() => {
        this.isPlaying = true;
        this.isPlayingFirst = true;
      }, 200);
      // console.log('durationchanged recieved', new Date());
      this.songLength = this.constants.songDuration;
      this.currentDuration = this.constants.currentsongDuration;

      // this.songLength = this.constants.dur;
      // this.currentDuration = this.constants.curDur;
      this.displayDur = this.constants.dur;
      this.displayCurrentDur = this.constants.curDur;
      if (Math.floor((this.constants.dur % 3600) / 60) >= 5) {
        if (Math.floor((this.constants.curDur % 3600) / 60) >= 5) {
          if (!this.present) {
            this.present = true;
            setTimeout(() => {
              if (this.constants.currentSong.is_provider_notified == "") {
                this.constants.currentSong.is_provider_notified = "no";
                this.showAlert();
              }
            }, 100);
          }
        }
      }
    });

    this.playService.pauseAndPlay().subscribe((val: any) => {
      console.log("-*HERE*-", val);
      if (document.getElementById("pause1Btn")) {
        document.getElementById("pause1Btn").click();
      }
      setTimeout(() => {
        if (document.getElementById("play1Btn")) {
          document.getElementById("play1Btn").click();
        }
        // setTimeout(() => {
        //   this.isPlaying = val;
        // }, 1000);
      }, 100);
    });
  }

  ionViewWillEnter() {
    // var that = this;
    // if (this.constants.songList.length) {
    //   this.total = this.constants.songList.length;
    //   var a = [];
    //   for (
    //     var i = this.comingUpnext.length;
    //     i < this.comingUpnext.length + 10;
    //     i++
    //   ) {
    //     // console.log('A:: ', i, ' - ', this.constants.songList[i]);
    //     a.push(this.constants.songList[i]);
    //   }
    //   // console.log('Records:: ', a);
    //   this.page++;
    //   this.comingUpnext = a;
    // }
  }

  triggerNextClick() {
    if (document.getElementById("nextBtn")) {
      console.log("Length:: ", this.constants.songList.length);
      if (this.constants.songList.length) {
        console.log("Getting here");
        this.nextEvent = true;
        document.getElementById("nextBtn").click();
      }
    } else {
      this.playService.stopSong();
      this.isPlaying = true;
      this.isPlayingFirst = true;
    }
  }

  loadMore() {
    var that = this;
    if (this.constants.songList.length) {
      this.total = this.constants.songList.length;
      var a = [];
      for (
        var i = this.comingUpnext.length;
        i < this.comingUpnext.length + 10;
        i++
      ) {
        console.log("A:: ", i, " - ", this.constants.songList[i]);
        a.push(this.constants.songList[i]);
      }
      // console.log('Records:: ', a);
      this.page++;
      a.forEach((element) => {
        that.comingUpnext.push(element);
      });
    }
  }

  fmtMSS(s) {
    if (s == 0) {
      return "0:00";
    }
    return s.toString().replace(".", ":");
    // return this.datePipe.transform( s,'mm:ss');
  }

  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor((d % 3600) / 60);
    var s = Math.floor((d % 3600) % 60);
    var hDisplay =
      h > 0
        ? (h.toString().length > 1 ? h : "0" + h) + (h == 1 ? ":" : ":")
        : "";
    var mDisplay =
      m > 0
        ? (m.toString().length > 1 ? m : "0" + m) + (m == 1 ? ":" : ":")
        : "00:";
    var sDisplay =
      s > 0
        ? (s.toString().length > 1 ? s : "0" + s) + (s == 1 ? "" : "")
        : "00";
    return hDisplay + mDisplay + sDisplay;
  }

  dismiss() {
    this.modalController.dismiss();
  }

  doNothing() {
    console.log("Do Nothing on click");
  }

  getCurrentSong() {
    // console.log(this.playService.currentPlayingSong, '@@@@@ ', this.playService.memoMedia);
    // console.log("Current songUrl:: ", this.constants.currentSong.songUrl);
    if (this.playService.memoMedia) {
      if (this.constants.status == "2") {
        if (
          this.playService.currentPlayingSong.ID ==
          this.constants.currentSong.ID
        ) {
          console.log("same song");
          this.playService.pauseSong();
        } else {
          this.playService.stopSong();
        }
      } else {
        this.playService.stopSong();
      }
    } else {
      this.playService.stopSong();
    }

    if (typeof this.constants.currentSong.songUrl == "object") {
      if (
        this.constants.currentSong.songUrl[
          Object.keys(this.constants.currentSong.songUrl)[0]
        ].indexOf("spotify") != -1
      ) {
        this.isSpotifyUrl = true;
        this.constants.currentSong.songUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
          this.constants.currentSong.songUrl[
            Object.keys(this.constants.currentSong.songUrl)[0]
          ]
        );
      } else {
        this.isSpotifyUrl = false;
        if (this.constants.play) {
          this.playSong();
        }
      }
    } else {
      if (this.constants.currentSong.songUrl.indexOf("spotify") != -1) {
        this.isSpotifyUrl = true;
        this.constants.currentSong.songUrl = this.constants.currentSong.songUrl.replace(
          "open.spotify.com",
          "embed.spotify.com"
        );
        this.constants.currentSong.songUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
          this.constants.currentSong.songUrl
        );
      } else {
        this.isSpotifyUrl = false;
        // if (this.constants.play) {
        this.playSong();
        // }
      }
    }

    this.currentSong = this.constants.currentSong;
    if (this.constants.currentSong.isLiked == "yes") {
      this.liked = true;
    } else {
      this.liked = false;
    }
    this.comingUpNextList();
  }

  comingUpNextList() {
    this.comingUpnext = [];
    // console.log(this.constants.currentSongIndex, 'this.constants.songList:: ', this.constants.songList);
    for (var i = 0; i < this.constants.songList.length; i++) {
      // console.log('Hello:: ', i, '\n --', this.constants.songList);
      if (i > this.constants.currentSongIndex) {
        let data: any = {};
        data = this.constants.songList[i];
        data.index = i;
        // console.log('Hello:: ', i, '\n --', data);
        this.comingUpnext.push(data);
      }
    }
    if (this.constants.currentSongIndex == this.constants.songList.length - 1) {
      for (var i = 0; i < this.constants.songList.length; i++) {
        if (i != this.constants.currentSongIndex) {
          let data: any = {};
          data = this.constants.songList[i];
          data.index = i;
          // console.log('Hello:: ', i, '\n --', data);
          this.comingUpnext.push(data);
        }
      }
    }
    // console.log("comingUpnext:: ", this.comingUpnext);
  }

  likeUnlikeSong() {
    var res = false;
    setTimeout(() => {
      if (!res) {
        this.api.presentLoading();
      }
    }, 2000);
    let url = "";
    if (this.liked) {
      url = "wp-json/wp/v2/unlike/song";
      this.constants.currentSong.isLiked = "no";
      this.constants.songList[this.constants.currentSongIndex].isLiked = "no";
    } else {
      url = "wp-json/wp/v2/like/song/";
      this.constants.currentSong.isLiked = "yes";
      this.constants.songList[this.constants.currentSongIndex].isLiked = "yes";
    }
    localStorage.setItem(
      "currentSong",
      JSON.stringify(this.constants.currentSong)
    );
    localStorage.setItem(
      "currentSongList",
      JSON.stringify(this.constants.songList)
    );
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: this.currentSong.ID,
    };
    // console.log("PARAM:: ", param);
    this.api.get(url, param).subscribe(
      (data: any) => {
        res = true;
        this.events.publish("likeUnlike", {});
        // console.log("Result::", data);
        if (data.status == "error") {
          this.api.presentToast("danger", data.message);
          return false;
        } else {
          if (this.liked) {
            this.liked = false;
          } else {
            this.liked = true;
          }
        }
        this.api.hideLoading();
      },
      (error: any) => {
        // console.log("Like unlike Error:: ", error);
        res = true;
        this.api.hideLoading();
        this.api.presentToast("danger", error);
      },
      () => {
        res = true;
        this.api.hideLoading();
        // console.log("Finally");
      }
    );
  }

  prevSong() {
    this.present = false;
    this.isPlaying = false;
    this.isPlayingFirst = false;
    this.displayDur = 0.0;
    this.displayCurrentDur = 0.0;
    this.currentDuration = 0.0;
    this.songLength = 0.0;
    this.playService.stopSong();
    if (this.constants.songList.length > 1) {
      if (this.constants.currentSongIndex == 0) {
        this.constants.currentSongIndex = this.constants.songList.length;
      }

      this.constants.currentSongIndex--;
      this.constants.currentSong = this.constants.songList[
        this.constants.currentSongIndex
      ];
      localStorage.setItem(
        "currentSong",
        JSON.stringify(this.constants.currentSong)
      );
      localStorage.setItem(
        "currentSongIndex",
        JSON.stringify(this.constants.currentSongIndex)
      );
      this.constants.songDuration = 0.0;
      this.constants.currentsongDuration = 0.0;
      this.getCurrentSong();
      // this.playSong();
      // if(this.currentSongIndex > 0) {
      //   this.currentSongIndex--;
      //   this.playSong();
      // }
    }
  }

  nextSong() {
    this.present = false;
    this.isPlaying = false;
    this.isPlayingFirst = false;
    // this.api.hideLoading();
    this.displayDur = 0.0;
    this.displayCurrentDur = 0.0;
    this.playService.stopSong();
    console.log(
      "currentSongIndex:: ",
      this.constants.currentSongIndex,
      this.constants.songList.length
    );

    if (this.constants.songList.length > 1) {
      if (
        this.constants.currentSongIndex ==
        this.constants.songList.length - 1
      ) {
        this.constants.currentSongIndex = -1;
      }

      this.constants.currentSongIndex = this.constants.currentSongIndex + 1;
      // this.constants.currentSongIndex = this.comingUpnext[0].index;
      console.log("currentSongIndex + 1 :: ", this.constants.currentSongIndex);
      this.constants.currentSong = this.constants.songList[
        this.constants.currentSongIndex
      ];
      this.constants.currentSong = this.comingUpnext[0];
      localStorage.setItem(
        "currentSong",
        JSON.stringify(this.constants.currentSong)
      );
      localStorage.setItem(
        "currentSongIndex",
        JSON.stringify(this.constants.currentSongIndex)
      );
      this.constants.songDuration = 0.0;
      this.constants.currentsongDuration = 0.0;
      this.constants.play = true;
      this.getCurrentSong();
      // }
      this.nextEvent = false;
    }
  }

  playNextSong(next, i) {
    this.present = false;
    this.isPlaying = false;
    this.isPlayingFirst = false;
    console.log("Next:: ", next);
    this.playService.stopSong();
    this.displayDur = 0.0;
    this.displayCurrentDur = 0.0;
    this.currentDuration = 0.0;
    this.songLength = 0.0;
    // this.constants.currentSong = this.constants.songList[next.index];
    this.constants.currentSong = next;
    console.log("songList:: ", this.constants.songList[next.index]);
    console.log("Current:: ", this.constants.currentSong);

    // this.constants.currentSongIndex = next.index;
    this.constants.currentSongIndex = i;
    localStorage.setItem("currentSong", JSON.stringify(next));
    // localStorage.setItem("currentSongIndex", JSON.stringify(next.index));
    localStorage.setItem("currentSongIndex", JSON.stringify(i));
    // this.getCurrentSong();
    // this.playSong();
    this.getCurrentSong();
  }

  option(song, index) {
    console.log('song:: ', song)
    var modal = this.modalController
      .create({ component: OptionPage, componentProps: { song: song, index: index } })
      .then((modalElement) => {
        modalElement.present();
        modalElement
          .onWillDismiss()
          .then((data) => {
            console.log("Modal dismiss success");
            if (this.constants.currentSong.isLiked == "yes") {
              this.liked = true;
            } else {
              this.liked = false;
            }
            // this.getCurrentSong();
          })
          .catch((err) => {
            console.log("Modal dismiss error");
          });
      });
  }

  toggleFab() {
    this.fabAction = !this.fabAction;
  }

  playSong() {
    // this.isPlaying = false;
    setTimeout(() => {
      // this.array_move(this.constants.currentSongIndex, 0);
      this.playService.playSong(this.constants.currentSong);
    }, 500);
  }

  seekSongTo(event) {
    // console.log('EVENT:: ', event.value);
    // console.log(
    //   "event:: ",
    //   event.value,
    //   "-",
    //   this.constants.currentsongDuration
    // );
    if (
      parseFloat(event.value) !== parseFloat(this.constants.currentsongDuration)
    ) {
      console.log('EVENT:: ', event.value);
      this.playService.seekSong(event.value);
    }
  }

  bannerImgError(evt) {
    // console.log("Banner image:: ", evt);
    this.currentSong.image = "assets/images/r1.png";
  }

  pauseSong() {
    // this.playsong = false;
    // this.constants.play = false;
    this.playService.pauseSong();
    // this.getSongStatus();
  }

  getSongStatus() {
    // console.log("getCurrentStatus:: ", this.playService.getCurrentStatus());
    // return this.playService.getCurrentStatus();
  }

  shuffle() {
    var currentIndex = this.constants.songList.length,
      temporaryValue,
      randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = this.constants.songList[currentIndex];
      this.constants.songList[currentIndex] = this.constants.songList[
        randomIndex
      ];
      this.constants.songList[randomIndex] = temporaryValue;
    }

    localStorage.setItem(
      "currentSongList",
      JSON.stringify(this.constants.songList)
    );

    for (var i = 0; i < this.constants.songList.length; i++) {
      if (this.constants.songList[i].ID == this.constants.currentSong.ID) {
        this.constants.currentSongIndex = i;
        localStorage.setItem(
          "currentSongIndex",
          JSON.stringify(this.constants.currentSongIndex)
        );
        break;
      }
    }
  }

  async showAlert() {
    const alert = await this.alert.create({
      cssClass: "my-custom-class",
      header: "Alert",
      backdropDismiss: false,
      message:
        "Can we let the podcast owner know that you've listened to their podcast?.",
      buttons: [
        {
          text: "No",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
            this.constants.currentSong.is_provider_notified = "no";
            this.constants.songList[
              this.currentSongIndex
            ].is_provider_notified = "no";
            localStorage.setItem(
              "currentSong",
              JSON.stringify(this.constants.currentSong)
            );
            localStorage.setItem(
              "currentSongList",
              JSON.stringify(this.constants.songList)
            );
            let url = "wp-json/wp/v2/user/listening/podcast";
            let param: any = {
              user_id: JSON.parse(localStorage.getItem("user")).user.id,
              post_id: this.constants.currentSong.ID,
              notify_provider: "no",
            };
            this.api.get(url, param).subscribe((data) => {
              if (data.status == "success") {
                console.log("Song added to recent played");
              }
            });
          },
        },
        {
          text: "yes",
          handler: () => {
            // https://zipi.honesttech.co.uk/wp-json/wp/v2/user/listening/podcast?series_id=118&user_id=58&notify_provider=yes
            console.log("Confirm Okay");
            let url = "wp-json/wp/v2/user/listening/podcast";
            this.constants.currentSong.is_provider_notified = "yes";
            this.constants.songList[
              this.currentSongIndex
            ].is_provider_notified = "yes";
            localStorage.setItem(
              "currentSong",
              JSON.stringify(this.constants.currentSong)
            );
            localStorage.setItem(
              "currentSongList",
              JSON.stringify(this.constants.songList)
            );
            let param: any = {
              user_id: JSON.parse(localStorage.getItem("user")).user.id,
              post_id: this.constants.currentSong.ID,
              notify_provider: "yes",
            };
            this.api.get(url, param).subscribe((data) => {
              if (data.status == "success") {
                console.log("Song added to recent played");
              }
            });
          },
        },
      ],
    });

    this.alert.getTop().then((val) => {
      if (val) {
        val.dismiss();
      }
    });
    setTimeout(async () => {
      await alert.present();
    }, 200);
  }

  array_move(old_index, new_index) {
    if (new_index >= this.comingUpnext.length) {
      var k = new_index - this.comingUpnext.length + 1;
      while (k--) {
        this.comingUpnext.push(undefined);
      }
    }
    this.constants.currentSongIndex = new_index;
    this.comingUpnext.splice(
      new_index,
      0,
      this.constants.songList.splice(old_index, 1)[0]
    );
    console.log("New Song List:: ", this.comingUpnext);
    // return this.constants.songList; // for testing
  }

  share() {
    var options = {
      message:
        "Listen to the song: " +
        this.constants.currentSong.post_title +
        " \nat " +
        this.constants.currentSong.guid +
        " on ZpiStream",
      chooserTitle: "Pick an app",
    };

    this.socialSharing
      .shareWithOptions(options)
      .then((data) => {
        console.log("Data shared:: ", data);
      })
      .catch((err) => {
        console.log("Error in data sharing:: ", err);
      });
  }

  openOptions(next) {

  }
}
