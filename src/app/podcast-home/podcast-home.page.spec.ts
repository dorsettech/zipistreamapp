import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PodcastHomePage } from './podcast-home.page';

describe('PodcastHomePage', () => {
  let component: PodcastHomePage;
  let fixture: ComponentFixture<PodcastHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PodcastHomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PodcastHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
