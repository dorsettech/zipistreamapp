import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { OptionPage } from "../option/option.page";
import { ApiService } from "../../services/api.service";
import { Constants } from "../../models/contants.models";
import { Events } from "../../services/events";
import { Router } from "@angular/router";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
@Component({
  selector: "app-podcast-home",
  templateUrl: "./podcast-home.page.html",
  styleUrls: ["./podcast-home.page.scss"],
})
export class PodcastHomePage implements OnInit, AfterViewInit {
  fabAction = false;
  songs: any = [];
  noResults: boolean = false;
  constructor(
    private modalController: ModalController,
    public apiService: ApiService,
    public constants: Constants,
    public events: Events,
    private route: Router,
    private splashScreen: SplashScreen
  ) {
    var that = this;
    this.events.subscribe("playlistUpdate", (data) => {
      setTimeout(() => {
        that.getAllSongs(false);
      }, 200);
    });
  }

  ngOnInit() {
  }
  
  ngAfterViewInit() {
    this.splashScreen.hide();
  }

  ionViewWillEnter() {
    console.log("PodcastHomePage ionViewWillEnter");
    this.getAllSongs(true);
  }

  /**
   * Playlist songs
   */
  getAllSongs(load) {
    if (load) {
      this.apiService.presentLoading();
    }
    // const url = "wp-json/wp/v2/all/songs";
    let url = "wp-json/wp/v2/playlist/songs";
    // const params = {search: this.searchVal};
    const params = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
    };
    this.apiService.get(url, params).subscribe(
      (val: any) => {
        console.log("val::: ", val);
        if (load) {
          this.apiService.hideLoading();
        }
        if (val.status == "success") {
          if (val.data.length) {
            this.noResults = false;
            this.songs = val.data;
            // this.getPlylistChannel(load);
          } else {
            this.noResults = true;
            this.songs = [];
            // this.getPlylistChannel(load);
          }
        } else {
          this.noResults = true;
          this.songs = [];
          // this.getPlylistChannel(load);
        }
      },
      (err: any) => {
        // if (load) {
        //   this.apiService.hideLoading();
        // }
        console.log("search Err:: ", err);
        this.noResults = true;
        this.songs = [];
        this.getPlylistChannel(load);
      }
    );
  }

  dismiss() {
    this.modalController.dismiss();
  }

  option() {
    this.modalController
      .create({ component: OptionPage })
      .then((modalElement) => {
        modalElement.present();
      });
  }

  toggleFab() {
    this.fabAction = !this.fabAction;
  }

  playSong(song, i) {
    console.log("Song:: ", song);
    this.constants.currentSong = song;
    this.constants.currentSongIndex = i;
    this.constants.songList = this.songs;
    setTimeout(() => {
      this.events.publish("songPlayed", song);
    }, 100);

    let url = "wp-json/wp/v2/song/played";
    let param: any = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: song.ID,
    };
    this.apiService.get(url, param).subscribe((data) => {
      // console.log(data);
      if (data.status == "success") {
        console.log("Song added to recent played");
      }
    });
  }

  gotoSetting() {
    this.route.navigate(["./setting"]);
  }

  playAll() {
    this.constants.currentSong = this.songs[0];
    this.constants.currentSongIndex = 0;
    this.constants.songList = this.songs;
    // this.events.publish("songPlayed", this.songs[0]);
    setTimeout(() => {
      this.events.publish("songPlayed", this.songs[0]);
    }, 100);
  }

  getPlylistChannel(load) {
    let url = "wp-json/wp/v2/channel/in/playlist";
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
    };
    console.log("PARAM:: ", param);
    this.apiService.get(url, param).subscribe(
      (data: any) => {
        if (load) {
          setTimeout(() => {
            this.apiService.hideLoading();
          }, 2000);
        }
        console.log("Result::", data);
        if (data.status == "error") {
          this.apiService.presentToast("danger", data.message);
          return false;
        } else {
          var that = this;
          data.data.forEach((element) => {
            that.getChannelSong(element, load);
          });
        }
      },
      (error: any) => {
        if (load) {
          this.apiService.hideLoading();
        }
        console.log("Playlist Error:: ", error);
        this.apiService.presentToast("danger", error);
      },
      () => {
        console.log("Finally");
      }
    );
  }

  getChannelSong(data, load) {
    let url = "wp-json/wp/v2/podcast/songs/";
    let params = { term_id: data.term_id };
    this.apiService.get(url, params).subscribe(
      (data) => {
        this.apiService.hideLoading();
        console.log("Podcasts data:: ", data);
        if (data.data) {
          // this.songs = data.data;
          var that = this;

          data.data.forEach((element) => {
            // that.songs.push(element);
            var result = that.songs.find((obj) => {
              return obj.ID === element.ID;
            });
            if (!result) {
              that.songs.push(element);
            }
          });
        } else {
        }
      },
      (err) => {
        this.apiService.hideLoading();
        console.log("featured error:: ", err);
      }
    );
  }

  removeFromPlaylist(song, i) {
    var res = false;
    setTimeout(() => {
      if (!res) {
        this.apiService.presentLoading();
      }
    }, 2000);
    let url = "wp-json/wp/v2/add/channel/to/playlist";
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      series_id: song.term_id,
    };
    console.log("PARAM:: ", param);
    this.apiService.get(url, param).subscribe(
      (data: any) => {
        this.apiService.hideLoading();
        res = true;
        this.songs.splice(i, 1);
        this.constants.songList = this.songs;

        localStorage.setItem(
          "currentSongList",
          JSON.stringify(this.constants.songList)
        );
        console.log("Result::", data);
        if (data.status == "error") {
          this.apiService.presentToast("danger", data.message);
          return false;
        } else {
          // this.getApi();
        }
      },
      (error: any) => {
        res = true;
        this.apiService.hideLoading();
        console.log("Playlist Error:: ", error);
        this.apiService.presentToast("danger", error);
      },
      () => {
        console.log("Finally");
      }
    );
  }
}
