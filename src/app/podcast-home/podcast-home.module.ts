import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PodcastHomePageRoutingModule } from './podcast-home-routing.module';

import { PodcastHomePage } from './podcast-home.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
	TranslateModule,  
    PodcastHomePageRoutingModule
  ],
  declarations: [PodcastHomePage]
})
export class PodcastHomePageModule {}
