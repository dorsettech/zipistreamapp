import { Component, OnInit, Inject } from "@angular/core";
import { NavController } from "@ionic/angular";
import { Router } from "@angular/router";
import { APP_CONFIG, AppConfig } from "../app.config";
import { PlayService } from "src/services/play.service";
import { Constants } from "src/models/contants.models";
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook/ngx";
import { ApiService } from "src/services/api.service";

@Component({
  selector: "app-setting",
  templateUrl: "./setting.page.html",
  styleUrls: ["./setting.page.scss"],
})
export class SettingPage implements OnInit {
  tab: string = "settigs";
  user: any = {};
  constructor(
    @Inject(APP_CONFIG) public config: AppConfig,
    private navCtrl: NavController,
    private route: Router,
    public playservice: PlayService,
    public constant: Constants,
    private googlePlus: GooglePlus,
    private fb: Facebook,
    private api: ApiService
  ) {
    this.user = JSON.parse(localStorage.getItem("user")).user;
  }

  ngOnInit() {}

  logout() {
    if (this.user.provider && this.user.provider == "google") {
      this.api.presentLoading();
      this.googlePlus
        .logout()
        .then(() => {
          this.api.hideLoading();
          this.playservice.stopSong();
          localStorage.clear();
          this.constant.resetVariables();
          this.navCtrl.navigateRoot(["./sign-in"]);
        })
        .catch((err) => {
          this.api.hideLoading();
          // this.api.presentToast("danger", JSON.stringify(err));
          this.playservice.stopSong();
          localStorage.clear();
          this.constant.resetVariables();
          this.navCtrl.navigateRoot(["./sign-in"]);
        });
    } else if (this.user.provider && this.user.provider == "facebook") {
      this.api.presentLoading();
      this.fb
        .logout()
        .then(() => {
          this.api.hideLoading();
          this.playservice.stopSong();
          localStorage.clear();
          this.constant.resetVariables();
          this.navCtrl.navigateRoot(["./sign-in"]);
        })
        .catch((err) => {
          this.api.hideLoading();
          // this.api.presentToast("danger", JSON.stringify(err));
          this.playservice.stopSong();
          localStorage.clear();
          this.constant.resetVariables();
          this.navCtrl.navigateRoot(["./sign-in"]);
        });
    } else {
      this.playservice.stopSong();
      localStorage.clear();
      this.constant.resetVariables();
      this.navCtrl.navigateRoot(["./sign-in"]);
    }
  }

  subscribe() {
    this.route.navigate(["./subscribe"]);
  }

  buyAppAction() {
    window.open("https://bit.ly/cc_Musicvic", "_system", "location=no");
  }
}
