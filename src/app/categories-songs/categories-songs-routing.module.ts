import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriesSongsPage } from './categories-songs.page';

const routes: Routes = [
  {
    path: '',
    component: CategoriesSongsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriesSongsPageRoutingModule {}
