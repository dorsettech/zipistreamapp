import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoriesSongsPageRoutingModule } from './categories-songs-routing.module';

import { CategoriesSongsPage } from './categories-songs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriesSongsPageRoutingModule
  ],
  declarations: [CategoriesSongsPage]
})
export class CategoriesSongsPageModule {}
