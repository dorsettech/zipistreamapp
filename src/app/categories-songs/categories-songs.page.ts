import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { PlaysongPage } from "../playsong/playsong.page";
import { ApiService } from "src/services/api.service";
import { Constants } from "../../models/contants.models";
import { NavController } from '@ionic/angular';
import { Events } from "../../services/events";
import { NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-categories-songs',
  templateUrl: './categories-songs.page.html',
  styleUrls: ['./categories-songs.page.scss'],
})
export class CategoriesSongsPage implements OnInit {
  parentId: any = '';
  podcast: any = [];
  songs: any = [];
  noRecord: boolean = false;
  constructor(
    private route: ActivatedRoute,
    public api: ApiService,
    public constants: Constants,
    public navCtrl: NavController,
    public events: Events
  ) {
    this.podcast = {};
    this.songs = [];
    this.route.queryParams.subscribe(params => {
      console.log('params:: ', params);
      this.parentId = params["parentId"];
      this.getPodcasts();
      // if(params['from'] == 'category') {
      //   this.parentId = params["parentId"];
      //   this.getPodcasts();
      // } else {
      //   this.podcast.description = params["description"];
      //   this.podcast.parentId = params["parentId"];
      //   this.podcast.taxonomy = params["taxonomy"];
      //   this.podcast.name = params["name"];
      //   this.podcast.term_id = parseInt(params["term_id"]);
      //   this.api.presentLoading();
      //   this.getSongs();
      // }
    });
    this.events.subscribe('categories-songs-back', (data) => {
      // this.goBack();
      if (document.getElementById('backbtn')) {
        document.getElementById('backbtn').click();
      }
    });
  }

  ngOnInit() {
  }

  getPodcasts() {
    this.api.presentLoading();
    let url = "wp-json/wp/v2/categories/podcast/";
    let params = {parent_id: this.parentId};
    this.api.get(url, params).subscribe(
      (data) => {
        console.log("Podcasts data:: ", data);
        if (data.data) {
          this.api.hideLoading();
          this.noRecord = false;
          this.podcast = data.data
          console.log('Podcasts:: ', data.data);
          // this.getSongs();
        } else {
          this.api.hideLoading();
          this.noRecord = true;
          this.podcast = {};
        }
      },
      (err) => {
        this.podcast = {};
        console.log("featured error:: ", err);
      });
  }

  getSongs() {
    let url = "wp-json/wp/v2/podcast/songs/";
    let params = {term_id: this.podcast.term_id};
    this.api.get(url, params).subscribe(
      (data) => {
        this.api.hideLoading();
        console.log("Podcasts data:: ", data);
        if (data.data) {
          this.songs = data.data
        } else {
          this.podcast = {};
        }
      },
      (err) => {
        this.api.hideLoading();
        this.podcast = {};
        console.log("featured error:: ", err);
      });
  }

  goBack() {
    this.navCtrl.back();
  }

  playSong(song, i) {
    console.log("Song:: ", song);
    this.constants.currentSong = song;
    this.constants.currentSongIndex = i;
    this.constants.songList = this.songs;
    this.events.publish("songPlayed", song);

    let url = "wp-json/wp/v2/song/played";
    let param: any = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: song.ID,
    };
    this.api.get(url, param).subscribe((data) => {
      if (data.status == "success") {
        console.log("Song added to recent played");
      }
    });
  }

  openPodcast(pod) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        parentId: (pod.parent),
        name: pod.name,
        description: pod.description,
        taxonomy: pod.taxonomy,
        term_id: pod.term_id,
        from: 'podcast',
        image: pod.image
      },
    };
    this.navCtrl.navigateForward(["tabs/podcast-songs"], navigationExtras);
  }

}
