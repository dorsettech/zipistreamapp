import { Component } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { PlaysongPage } from "../playsong/playsong.page";
import { Constants } from "../../models/contants.models";
import { Events } from "../../services/events";
import { PlayService } from "src/services/play.service";
import { OptionPage } from "../option/option.page";

@Component({
  selector: "app-tabs",
  templateUrl: "tabs.page.html",
  styleUrls: ["tabs.page.scss"],
})
export class TabsPage {
  favorite_icon = false;
  showCurrentSong: boolean = false;
  constructor(
    private modalController: ModalController,
    public constants: Constants,
    public events: Events,
    public playService: PlayService
  ) {
    if (Object.keys(constants.currentSong).length) {
      this.showCurrentSong = true;
    }
    this.events.subscribe("songPlayed", (data) => {
      console.log("HERE", data, "\n DATA:: ", constants.currentSong);
      if (Object.keys(constants.currentSong).length) {
        this.openPlayer();
        this.showCurrentSong = true;
        // this.constants.play = true;
        localStorage.setItem(
          "currentSong",
          JSON.stringify(constants.currentSong)
        );
        localStorage.setItem(
          "currentSongList",
          JSON.stringify(constants.songList)
        );
        localStorage.setItem(
          "currentSongIndex",
          JSON.stringify(constants.currentSongIndex)
        );
      }
    });
  }

  toggleFavorite_icon() {
    this.favorite_icon = !this.favorite_icon;
  }

  openPlayer() {
    // this.constants.isPlaying = false;
    console.log('::OPEN PLAYER:: ');

    this.modalController.getTop().then((modal) => {
      console.log('MODAL PLAYER:: ', modal);
      if (modal) {
        modal.dismiss();
      }
    });
    setTimeout(() => {
      this.modalController
        .create({ component: PlaysongPage })
        .then((modalElement) => {
          modalElement
            .present()
            .then((data) => {
              console.log("Modal present:: ", data);
              this.constants.isPlayerVisible = true;
            })
            .catch((err) => {
              this.constants.isPlayerVisible = false;
            });

          modalElement
            .onWillDismiss()
            .then(() => {
              console.log("modal dismissed");
              this.constants.isPlayerVisible = false;
            })
            .catch((err) => {
              console.log("modal dismissed error", err);
            });
        });
    }, 100);
  }

  playsong() {
    // this.constants.play = true;
    // this.events.publish('playsong', {});
    console.log("==", this.constants.currentSong.songUrl);
    this.playService.playSong(this.constants.currentSong);
  }

  pausesong() {
    // this.constants.play = false;
    // this.events.publish('pausesong', {});
    this.playService.pauseSong();
  }

  option() {
    var modal = this.modalController
      .create({ component: OptionPage, componentProps: { song: this.constants.currentSong, index: this.constants.currentSongIndex } })
      .then((modalElement) => {
        modalElement.present();
        modalElement
          .onWillDismiss()
          .then((data) => {
            console.log("Modal dismiss success");
            // if (this.constants.currentSong.isLiked == "yes") {
            //   this.liked = true;
            // } else {
            //   this.liked = false;
            // }
            // this.getCurrentSong();
          })
          .catch((err) => {
            console.log("Modal dismiss error");
          });
      });
  }
}
