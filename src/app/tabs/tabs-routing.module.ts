import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'search',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../search/search.module').then(m => m.SearchPageModule)
          }
        ]
      },
      {
        path: 'radio',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../radio/radio.module').then(m => m.RadioPageModule)
          }
        ]
      },
		{
        path: 'playlist',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../playlist/playlist.module').then(m => m.PlaylistPageModule)
          }
        ]
      },
      {
        path: 'add-song',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../add-song/add-song.module').then(m => m.AddSongPageModule)
          }
        ]
      },
      {
        path: 'podcast-home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../podcast-home/podcast-home.module').then(m => m.PodcastHomePageModule)
          }
        ]
      },

      {
        path: 'categories-songs',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../categories-songs/categories-songs.module').then(m => m.CategoriesSongsPageModule)
          }
        ]
      },

      {
        path: 'podcast-songs',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../podcast-songs/podcast-songs.module').then(m => m.PodcastSongsPageModule)
          }
        ]
      },

      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
