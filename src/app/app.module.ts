import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { TranslateLoader, TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OptionPageModule } from './option/option.module';
import { CreateplaylistPageModule } from './createplaylist/createplaylist.module';
import { PlaysongPageModule } from './playsong/playsong.module';
import { StoriesPageModule } from './stories/stories.module';
import { APP_CONFIG, BaseAppConfig } from './app.config';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
// import { Media, MediaObject } from '@ionic-native/media/ngx';
import { Constants } from 'src/models/contants.models';
import { Media, MediaObject } from "@ionic-native/media/ngx";

import { Events } from '../services/events';
import { DatePipe } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
// import { MusicControls } from '@ionic-native/music-controls/ngx';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


@NgModule({
  declarations: [AppComponent, 
    // Events,
  ],
  entryComponents: [
    // Events,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    OptionPageModule,
    PlaysongPageModule,
    CreateplaylistPageModule,
    StoriesPageModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativePageTransitions, 
    Media, 
    // MusicControls,
    Constants,DatePipe, SocialSharing, GooglePlus, Facebook,
    { provide: APP_CONFIG, useValue: BaseAppConfig },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
