import { Component, OnInit } from "@angular/core";
import { NavController } from "@ionic/angular";
import { ApiService } from "../../services/api.service";
import { Constants } from "../../models/contants.models";
import { Events } from "../../services/events";
import { NavigationExtras } from "@angular/router";

declare var window: any;
@Component({
  selector: "app-add-song",
  templateUrl: "./add-song.page.html",
  styleUrls: ["./add-song.page.scss"],
})
export class AddSongPage implements OnInit {
  tab: string = "search";
  searchVal: string = "";
  searchRes: any = [];
  showRes: boolean = false;
  likedSongs: any = [];
  recentlyPlayed: any = [];
  showLoader: boolean = false;

  constructor(
    public apiService: ApiService,
    public constants: Constants,
    public events: Events,
    public navCtrl: NavController
  ) {
    var that = this;
    this.events.subscribe("likeUnlike", (data) => {
      setTimeout(() => {
        that.getLikedSongs(false);
      }, 200);
    });

    this.events.subscribe("playlistUpdate", (data) => {
      setTimeout(() => {
        // that.getLikedSongs(false);
        if (this.tab == "search") {
          // this.recentSearched(false);
          this.getPlylistChannel(false);
        }

        if (this.tab == "you_liked") {
          this.getLikedSongs(false);
        }

        if (this.tab == "recentil_played") {
          this.recentlyPlayedSongs(false);
        }
      }, 200);
    });
  }

  ngOnInit() {
    // this.recentSearched();
    // var playBtn = document.querySelector("iframe#playbtn");
    // playBtn[0].postMessage("play", "http://embed.spotify.com/"); /// Will play
    // playBtn[0].postMessage("stop", "http://embed.spotify.com/"); /// Will play
    // window.onmessage = function (e) {
    //   if (e.origin == "http://embed.spotify.com") {
    //     if (e.data.action == "playbackStarted") {
    //       /* On playback started */
    //     }
    //   }
    // };
  }

  ionViewWillEnter() {
    console.log("AddSongPage ionViewWillEnter");
    // this.recentSearched();
    this.searchVal = "";
    if (this.tab == "search") {
      // this.recentSearched(true);
      this.getPlylistChannel(true);
    }

    if (this.tab == "you_liked") {
      this.getLikedSongs(true);
    }

    if (this.tab == "recentil_played") {
      this.recentlyPlayedSongs(true);
    }
  }

  tabChanged(event) {
    console.log("Segment changed:: ", this.tab);
    if (this.tab == "search") {
      // this.recentSearched(true);
      this.getPlylistChannel(true);
    } else if (this.tab == "you_liked") {
      this.getLikedSongs(true);
    } else {
      this.recentlyPlayedSongs(true);
    }
  }

  getAllSongs() {}

  clearSearch() {
    this.searchVal = '';
    this.searchRes = [];
    this.showRes = false;
    // this.recentSearched(true);
    this.getPlylistChannel(true);
  }

  searchInput() {
    // this.searchVal = '';
    console.log("Search Val:: ", this.searchVal);
    if (this.searchVal.length) {
      // const url = "wp-json/wp/v2/get/search";
      this.searchRes = [];
      this.showLoader = true;
      const url = "wp-json/wp/v2/podcast/search";
      const params = { search: this.searchVal };
      this.apiService.get(url, params).subscribe(
        (val: any) => {
          console.log("val::: ", val);
          if (val.status == "success") {
            this.showLoader = false;
            this.searchRes = val.data && val.data.length ? val.data : [];
            if (this.searchRes.length) {
              this.showRes = true;
            } else {
              this.showRes = false;
            }
          } else {
            this.showLoader = false;
            this.searchRes = [];
          }
        },
        (err: any) => {
          this.showLoader = false;
          console.log("search Err:: ", err);
          this.showRes = false;
        }
      );
    } else {
      this.searchRes = [];
      this.showRes = false;
      // this.recentSearched(true);
      this.getPlylistChannel(true);
    }
  }

  getLikedSongs(load) {
    this.searchVal = "";
    if (load) {
      this.apiService.presentLoading();
    }
    const url = "wp-json/wp/v2/liked/song/list";
    // const params = {search: this.searchVal};
    const params = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
    };
    this.apiService.get(url, params).subscribe(
      (val: any) => {
        console.log("val::: ", val);
        if (load) {
          this.apiService.hideLoading();
        }
        if (val.status == "success") {
          if (val.data.length) {
            this.likedSongs = val.data;
          } else {
            this.likedSongs = [];
          }
        } else {
          this.likedSongs = [];
        }
      },
      (err: any) => {
        if (load) {
          this.apiService.hideLoading();
        }
        console.log("search Err:: ", err);
        this.likedSongs = [];
      }
    );
  }

  recentlyPlayedSongs(load) {
    this.searchVal = "";
    if (load) {
      this.apiService.presentLoading();
    }
    const url = "wp-json/wp/v2/recent/played";
    const params = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
    };
    this.apiService.get(url, params).subscribe(
      (val: any) => {
        console.log("val::: ", val);
        if (load) {
          this.apiService.hideLoading();
        }
        if (val.status == "success") {
          if (val.data.length) {
            this.recentlyPlayed = val.data;
          } else {
            this.recentlyPlayed = [];
          }
        } else {
          this.recentlyPlayed = [];
        }
      },
      (err: any) => {
        if (load) {
          this.apiService.hideLoading();
        }
        console.log("search Err:: ", err);
        this.recentlyPlayed = [];
      }
    );
  }

  recentSearched(load) {
    this.searchVal = "";
    if (load) {
      this.apiService.presentLoading();
    }
    // const url = "wp-json/wp/v2/recent/searched";
    const url = "wp-json/wp/v2/recent/searched/podcasts";
    const params = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
    };
    this.apiService.get(url, params).subscribe(
      (val: any) => {
        console.log("val::: ", val);
        if (load) {
          this.apiService.hideLoading();
        }
        if (val.status == "success") {
          if (val.data.length) {
            this.searchRes = val.data;
          } else {
            this.searchRes = [];
          }
        } else {
          this.searchRes = [];
        }
      },
      (err: any) => {
        if (load) {
          this.apiService.hideLoading();
        }
        console.log("search Err:: ", err);
        this.searchRes = [];
      }
    );
  }

  addtoRecentSearch(pod) {
    // let url = "wp-json/wp/v2/search/podcast/clicked";
    let url = "wp-json/wp/v2/podcast/search/clicked";
    let param: any = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      series_id: pod.term_id,
    };
    this.apiService.get(url, param).subscribe((data) => {
      // console.log(data);
      if (data.status == "success") {
        console.log("Podcasts added to recent search");
      }
    });
  }

  playSong(song, i) {
    this.searchVal = "";
    console.log("Song:: ", song, "\n i:: ", i);
    this.constants.currentSong = song;
    this.constants.currentSongIndex = i;

    if (this.tab == "search") {
      let url = "wp-json/wp/v2/search/clicked";
      let param: any = {
        user_id: JSON.parse(localStorage.getItem("user")).user.id,
        post_id: song.ID,
      };
      this.apiService.get(url, param).subscribe((data) => {
        // console.log(data);
        if (data.status == "success") {
          console.log("Song added to recent search");
        }
      });

      this.constants.songList = this.searchRes;
    }

    if (this.tab == "you_liked") {
      this.constants.songList = this.likedSongs;
    }

    if (this.tab == "recentil_played") {
      console.log("Here:: ", this.recentlyPlayed);
      this.constants.songList = this.recentlyPlayed;
    }

    // this.events.publish("songPlayed", song);
    setTimeout(() => {
      this.events.publish("songPlayed", song);
    }, 100);

    let url = "wp-json/wp/v2/song/played";
    let param: any = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: song.ID,
    };
    this.apiService.get(url, param).subscribe((data) => {
      // console.log(data);
      if (data.status == "success") {
        console.log("Song added to recent played");
      }
    });
  }

  chooseSearch(res) {
    console.log(res);
    this.showRes = false;
    this.searchVal = res.post_title;
  }

  blurSearch() {
    setTimeout(() => {
      this.showRes = false;
    }, 100);
  }

  addToPlaylist(song) {
    // this.searchVal = '';
    this.apiService.presentLoading();
    console.log("searchVal:: ", this.searchVal);
    let url = "";
    if (song.isAddedToPlaylist == "yes") {
      url = "wp-json/wp/v2/remove/from/playlist";
    } else {
      url = "wp-json/wp/v2/add/to/playlist";
    }
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      post_id: song.ID,
    };
    console.log("PARAM:: ", param);
    this.apiService.get(url, param).subscribe(
      (data: any) => {
        this.apiService.hideLoading();
        if (song.isAddedToPlaylist == "yes") {
          song.isAddedToPlaylist = "no";
        } else {
          song.isAddedToPlaylist = "yes";
        }
        console.log("Result::", data);
        if (data.status == "error") {
          this.apiService.presentToast("danger", data.message);
          return false;
        } else {
        }
      },
      (error: any) => {
        this.apiService.hideLoading();
        console.log("Playlist Error:: ", error);
        this.apiService.presentToast("danger", error);
      },
      () => {
        console.log("Finally");
      }
    );
  }

  addPodcastToPlaylist(song) {
    // this.searchVal = '';
    var res = false;
    setTimeout(() => {
      if(!res) {
        this.apiService.presentLoading();
      }
    }, 2000);
    console.log("searchVal:: ", this.searchVal);
    let url = "";
    if (song.isAddedToPlaylist == "yes") {
      url = "wp-json/wp/v2/remove/channel/from/playlist";
    } else {
      url = "wp-json/wp/v2/add/channel/to/playlist";
    }
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
      series_id: song.term_id,
    };
    console.log("PARAM:: ", param);
    this.apiService.get(url, param).subscribe(
      (data: any) => {
        this.apiService.hideLoading();
        res = true;
        if (song.isAddedToPlaylist == "yes") {
          song.isAddedToPlaylist = "no";
        } else {
          song.isAddedToPlaylist = "yes";
        }
        console.log("Result::", data);
        if (data.status == "error") {
          this.apiService.presentToast("danger", data.message);
          return false;
        } else {
          this.getApi();
        }
      },
      (error: any) => {
        res = true;
        this.apiService.hideLoading();
        console.log("Playlist Error:: ", error);
        this.apiService.presentToast("danger", error);
      },
      () => {
        console.log("Finally");
      }
    );
  }

  getApi() {
    let url = "wp-json/wp/v2/channel/in/playlist";
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
    };
    console.log("PARAM:: ", param);
    this.apiService.get(url, param).subscribe(
      (data: any) => {
        this.apiService.hideLoading();
        console.log("Result::", data);
        if (data.status == "error") {
          this.apiService.presentToast("danger", data.message);
          return false;
        } else {
        }
      },
      (error: any) => {
        this.apiService.hideLoading();
        console.log("Playlist Error:: ", error);
        this.apiService.presentToast("danger", error);
      },
      () => {
        console.log("Finally");
      }
    );
  }

  openPodcasts(pod, i) {
    this.addtoRecentSearch(pod);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        parentId: pod.parent,
        name: pod.name,
        description: pod.description,
        taxonomy: pod.taxonomy,
        term_id: pod.term_id,
        from: "podcast",
        image: pod.image,
      },
    };
    this.navCtrl.navigateForward(["tabs/podcast-songs"], navigationExtras);
  }

  getPlylistChannel(load) {
    if (load) {
      this.apiService.presentLoading();
    }
    let url = "wp-json/wp/v2/channel/in/playlist";
    var param = {
      user_id: JSON.parse(localStorage.getItem("user")).user.id,
    };
    console.log("PARAM:: ", param);
    this.apiService.get(url, param).subscribe(
      (data: any) => {
        if (load) {
          setTimeout(() => {
            this.apiService.hideLoading();
          }, 2000);
        }
        console.log("Result::", data);
        if (data.status == "error") {
          this.searchRes = [];
          this.apiService.presentToast("danger", data.message);
          return false;
        } else {
          var that = this;
          if (data.data && data.data.length) {
            this.searchRes = data.data;
          } else {
            this.searchRes = [];
          }
        }
      },
      (error: any) => {
        this.searchRes = [];
        if (load) {
          this.apiService.hideLoading();
        }
        console.log("Playlist Error:: ", error);
        this.apiService.presentToast("danger", error);
      },
      () => {
        console.log("Finally");
      }
    );
  }
}
