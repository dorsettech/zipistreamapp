import { Component, Inject, Renderer2 } from "@angular/core";
import { Platform, NavController, ModalController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { TranslateService } from "@ngx-translate/core";
import { Constants } from "src/models/contants.models";
import { APP_CONFIG, AppConfig } from "./app.config";
import { MyEvent } from "src/services/myevent.services";
import { DOCUMENT } from "@angular/common";
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook/ngx";
import { Router } from '@angular/router';
import { Events } from '../services/events'
import { PlayService } from "src/services/play.service";

declare var window: any;
@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  rtlSide = "left";

  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private platform: Platform,
    private navCtrl: NavController,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private myEvent: MyEvent,
    private _renderer2: Renderer2,
    public constants: Constants,
    private googlePlus: GooglePlus,
    private fb: Facebook,
    public router: Router,
    public modalController: ModalController,
    private event: Events,
    private playService: PlayService
  ) {
    this.initializeApp();
    this.myEvent.getLanguageObservable().subscribe((value) => {
      //this.navCtrl.navigateRoot(['./']);
      this.globalize(value);
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      // this.splashScreen.hide();
      var that = this;
      setTimeout(() => {
        this.platform.backButton.subscribe(async () => {
          console.log('/tabs/podcast-songs:: ', this.router.isActive('/tabs/podcast-songs', true));
          console.log('/tabs/categories-songs:: ', this.router.isActive('/tabs/categories-songs', true));
          console.log('backButton:: ', that.router.getCurrentNavigation(), '--', that.router.url);
          var url = (that.router.url).split('?')[0];
          if(that.constants.isPlayerVisible) {
            that.modalController.getTop().then((modal) => {
              console.log('MODAL PLAYER:: ', modal);
              if (modal) {
                modal.dismiss();
                // this.platform.backButton.unsubscribe();
                return false;
              }
            });
          }

          if(url.indexOf('/tabs/podcast-songs') > -1 ) {
            console.log('::GoBACK::');
            window.history.back();
            // that.event.publish('podcast-songs-back', {});
            // this.platform.backButton.unsubscribe();
          }

          if(url.indexOf('/tabs/categories-songs') > -1) {
            console.log('::GoBACK::');
            window.history.back();
            // that.event.publish('categories-songs-back', {});
            // this.platform.backButton.unsubscribe();
          }
        });
      }, 100)

      let defaultLang = window.localStorage.getItem(
        Constants.KEY_DEFAULT_LANGUAGE
      );
      this.globalize(defaultLang);
      if (localStorage.getItem("user") !== null) {
        var user = JSON.parse(localStorage.getItem("user"));
        if(user.provider == 'google') {
          this.googlePlus.trySilentLogin().then((val) => {
            console.log('Silent login:: ', val);
          }).catch((err) => {
            console.log('Silent login error:: ', err);
          });
        }
        // if(user.provider == 'facebook') {
        //   this.fb.getLoginStatus().then((val) => {

        //   })
        // }
        if (
          localStorage.getItem("currentSong") !== null &&
          localStorage.getItem("currentSong") !== undefined &&
          localStorage.getItem("currentSong") !== "undefined"
        ) {
          this.constants.currentSong = JSON.parse(
            localStorage.getItem("currentSong")
          );
          // this.playService.playMusicControls(this.constants.currentSong);
        } else {
          localStorage.removeItem("currentSong");
        }

        if (
          localStorage.getItem("currentSongList") !== null &&
          localStorage.getItem("currentSongList") !== undefined &&
          localStorage.getItem("currentSongList") !== "undefined"
        ) {
          this.constants.songList = JSON.parse(
            localStorage.getItem("currentSongList")
          );
        } else {
          localStorage.removeItem("currentSongList");
        }

        if (
          localStorage.getItem("currentSongIndex") !== null &&
          localStorage.getItem("currentSongIndex") !== undefined &&
          localStorage.getItem("currentSongIndex") !== "undefined"
        ) {
          this.constants.currentSongIndex = JSON.parse(
            localStorage.getItem("currentSongIndex")
          );
        } else {
          localStorage.removeItem("currentSongIndex");
        }
        this.navCtrl.navigateRoot(["/tabs/podcast-home"]);
      } else {
        this.navCtrl.navigateRoot(["/"]);
      }
    });
  }

  globalize(languagePriority) {
    this.translate.setDefaultLang("en");
    let defaultLangCode = this.config.availableLanguages[0].code;
    this.translate.use(
      languagePriority && languagePriority.length
        ? languagePriority
        : defaultLangCode
    );
    this.setDirectionAccordingly(
      languagePriority && languagePriority.length
        ? languagePriority
        : defaultLangCode
    );
  }

  setDirectionAccordingly(lang: string) {
    switch (lang) {
      case "ar": {
        this.rtlSide = "rtl";
        break;
      }
      default: {
        this.rtlSide = "ltr";
        break;
      }
    }
  }
}
